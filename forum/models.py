from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify

from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from django.core.urlresolvers import reverse

# Create your models here.


def forum_image_upload(instance, filename):
    return "img/forum/%s/%s" % (instance.user.username, filename)


class ForumMainCategory(models.Model):
    """
        main category of forums
    """
    category = models.CharField(max_length=100, primary_key=True)
    order = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return self.category

    class Meta:
        ordering = ['order']

class ForumSubCategory(models.Model):
    """
        Sub category from Main
    """
    main = models.ForeignKey(ForumMainCategory, on_delete=models.CASCADE, related_name='main')
    category = models.CharField(max_length=100, primary_key=True)

    post_count = models.IntegerField(default=0)
    reply_count = models.IntegerField(default=0)
    like_count = models.IntegerField(default=0)

    slug = models.SlugField(unique=True, null=True)
    order = models.IntegerField(null=True, blank=True)

    def __unicode__(self):
        return self.category

    class Meta:
        ordering = ['order', 'category']


class ForumPost(models.Model):
    """
        forum post
    """
    title = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='forum_user')

    post = models.TextField()

    reply_count = models.IntegerField(default=0)
    like_count = models.IntegerField(default=0)
    view_count = models.IntegerField(default=0)

    sub_category = models.ForeignKey(ForumSubCategory, on_delete=models.CASCADE, related_name='sub_cat')

    def __unicode__(self):
        return self.title


class ForumPostComment(models.Model):
    """
        comments for forum post
    """

    post = models.ForeignKey(ForumPost, on_delete=models.CASCADE, related_name='post_comment')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='post_comment_user')
    comment = models.TextField()
    date = models.DateTimeField(auto_now=False, auto_now_add=True)
    like = models.IntegerField(default=0)

    reply = models.ForeignKey('self', null=True, blank=True, default=None)

    def __unicode__(self):
        return self.comment


class ForumPostImage(models.Model):
    """
        Image for forum post
    """

    user = models.ForeignKey(User)
    date = models.DateTimeField(auto_now=False, auto_now_add=True)

    image_used = models.BooleanField(default=True)

    image = models.ImageField(upload_to=forum_image_upload, null=True, blank=True)
    thumbnail_image = models.ImageField(upload_to=forum_image_upload, null=True, blank=True)

    def __unicode__(self):
        return self.thumbnail_image