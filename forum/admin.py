from django.contrib import admin
from .models import ForumMainCategory, ForumSubCategory, ForumPost, ForumPostComment, ForumPostImage
# Register your models here.

admin.site.register(ForumMainCategory)
admin.site.register(ForumSubCategory)
admin.site.register(ForumPost)
admin.site.register(ForumPostComment)
admin.site.register(ForumPostImage)