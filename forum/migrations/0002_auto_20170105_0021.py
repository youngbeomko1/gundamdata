# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='forummaincategory',
            name='category',
            field=models.CharField(max_length=100, serialize=False, primary_key=True),
        ),
    ]
