# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0006_auto_20170209_0717'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='forummaincategory',
            options={'ordering': ['order']},
        ),
        migrations.AlterModelOptions(
            name='forumsubcategory',
            options={'ordering': ['order', 'category']},
        ),
        migrations.AddField(
            model_name='forummaincategory',
            name='order',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
