# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ForumMainCategory',
            fields=[
                ('category', models.CharField(max_length=10, serialize=False, primary_key=True)),
            ],
        ),
    ]
