# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0003_auto_20170105_0128'),
    ]

    operations = [
        migrations.AddField(
            model_name='forumsubcategory',
            name='order',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
