# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import forum.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('forum', '0004_forumsubcategory_order'),
    ]

    operations = [
        migrations.CreateModel(
            name='ForumPostComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.TextField()),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('like', models.IntegerField(default=0)),
                ('post', models.ForeignKey(related_name='post_comment', to='forum.ForumPost')),
                ('reply', models.ForeignKey(default=None, blank=True, to='forum.ForumPostComment', null=True)),
                ('user', models.ForeignKey(related_name='post_comment_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ForumPostImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('image_used', models.BooleanField(default=True)),
                ('image', models.ImageField(null=True, upload_to=forum.models.forum_image_upload, blank=True)),
                ('thumbnail_image', models.ImageField(null=True, upload_to=forum.models.forum_image_upload, blank=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
