# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('forum', '0002_auto_20170105_0021'),
    ]

    operations = [
        migrations.CreateModel(
            name='ForumPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('post', models.TextField()),
                ('reply_count', models.IntegerField(default=0)),
                ('like_count', models.IntegerField(default=0)),
                ('view_count', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='ForumSubCategory',
            fields=[
                ('category', models.CharField(max_length=100, serialize=False, primary_key=True)),
                ('post_count', models.IntegerField(default=0)),
                ('reply_count', models.IntegerField(default=0)),
                ('like_count', models.IntegerField(default=0)),
                ('slug', models.SlugField(unique=True, null=True)),
                ('main', models.ForeignKey(related_name='main', to='forum.ForumMainCategory')),
            ],
        ),
        migrations.AddField(
            model_name='forumpost',
            name='sub_category',
            field=models.ForeignKey(related_name='sub_cat', to='forum.ForumSubCategory'),
        ),
        migrations.AddField(
            model_name='forumpost',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
