from rest_framework.serializers import (
    ModelSerializer,
    HyperlinkedIdentityField,
    SerializerMethodField,
    ValidationError,
    HyperlinkedModelSerializer,
    HyperlinkedRelatedField,
    EmailField,
    CharField,
    BooleanField,
    ReadOnlyField
)

from forum.models import ForumMainCategory, ForumSubCategory, ForumPost, ForumPostComment, ForumPostImage

from account.api.serializers import UserPostInfoSerializer

from django.contrib.auth import get_user_model


User = get_user_model()


class ForumMainPageSubSerializer(ModelSerializer):
    """
        sub category for Main
    """

    first_post = SerializerMethodField()

    class Meta:
        model = ForumSubCategory
        fields = [
            'category',
            'slug',
            'post_count',
            'reply_count',
            'first_post'
        ]

    def get_first_post(self, obj):
        qs = ForumPost.objects.filter(sub_category=obj)
        sub = qs.order_by('-date')
        try:
            title = sub[0].title
            date = sub[0].date
            username = sub[0].user.username
            image = sub[0].user.account.profile_image.url
            pk = sub[0].user.account.pk
            sub = {'title': title, 'date': date, 'username': username, 'image': image, 'pk': pk}
        except:
            sub = None
        return sub


class ForumMainPageSerializer(ModelSerializer):
    """
        Serializer for Forum main page
    """

    sub_category_list = ForumMainPageSubSerializer(source='main', many=True, read_only=True)

    class Meta:
        model = ForumMainCategory
        fields = [
            'category',
            'sub_category_list'
        ]


class ForumRecentPostSerializer(ModelSerializer):
    """
        recent post in all forum post
    """

    user = ReadOnlyField(source='user.username')

    class Meta:
        model = ForumPost
        fields = [
            'title',
            'date',
            'reply_count',
            'user',
            'pk'
        ]


class ForumRecentMainSerializer(ModelSerializer):
    """
        Recent + main page serializer
    """

    recent = SerializerMethodField()
    main = SerializerMethodField()

    class Meta:
        model = ForumMainCategory
        fields = [
            'recent',
            'main'
        ]

    #def get_recent(self):
    def get_recent(self, obj):
        query = ForumPost.objects.order_by('-date')[:4]
        serializer = ForumRecentPostSerializer(query, many=True)
        return serializer.data

    #def get_main(self):
    def get_main(self, obj):
        query = ForumMainCategory.objects.all()
        serializer = ForumMainPageSerializer(query, many=True)
        return serializer.data






class ForumSubCategoryInfoSerializer(ModelSerializer):
    """
        Serializer for sub category information dependency
    """

    sub_category_link = HyperlinkedIdentityField(
        view_name='forum-api:sub_category',
        lookup_field='slug'
    )

    class Meta:
        model = ForumSubCategory
        fields = [
            'main',
            'category',
            'post_count',
            'reply_count',
            'like_count',
            'slug',
            'order',
            'sub_category_link'
        ]


class ForumCategorySerializer(ModelSerializer):
    """
        Serializer for category
    """

    sub_category_list = ForumSubCategoryInfoSerializer(source='main', many=True, read_only=True)

    class Meta:
        model = ForumMainCategory
        fields = [
            'category',
            'sub_category_list',
        ]


class ForumCommentSerializer(ModelSerializer):
    """
        serializer for forum comments
    """

    user_profile = UserPostInfoSerializer(source='user.account', read_only=True)

    class Meta:
        model = ForumPostComment
        fields = [
            'post',
            'user',
            'comment',
            'date',
            'like',
            'reply',
            'user_profile'
        ]
        read_only_fields = [
            'user',
            'like',
            'post',
        ]

class ForumLatestCommentSerializer(ModelSerializer):
    """
        serializer for forum latest comments in list
    """

    user_profile = UserPostInfoSerializer(source='user.account', read_only=True)

    class Meta:
        model = ForumPostComment
        fields = [
            'date',
            'like',
            'user_profile'
        ]
        read_only_fields = [
            'like',
            'post',
        ]



class ForumPostSerializer(ModelSerializer):
    """
        Serializer for post on a forum
    """
    user_profile = UserPostInfoSerializer(source='user.account', read_only=True)
    comments_link = HyperlinkedIdentityField(
        view_name='forum-api:forum_comment',
        lookup_field='pk'
    )

    slug = ReadOnlyField(source='sub_category.slug')


    class Meta:
        model = ForumPost
        fields = [
            'title',
            'pk',
            'date',
            'user',
            'post',
            'comments_link',
            'reply_count',
            'like_count',
            'view_count',
            'sub_category',
            'slug',
            'user_profile',

        ]
        read_only_fields = [
            'reply_count',
            'pk',
            'like_count',
            'view_count',
            'user_profile',
            'date',
            'user',
            'comments_link'
        ]


class ForumPostInfoSerializer(ModelSerializer):
    """
        serializer for post info for category (used for forum post list)
    """

    user_profile = UserPostInfoSerializer(source='user.account', read_only=True)
    post_link = HyperlinkedIdentityField(
        view_name='forum-api:forum_post',
        lookup_field='pk'
    )
    comments_link = HyperlinkedIdentityField(
        view_name='forum-api:forum_comment',
        lookup_field='pk'
    )

    recent_reply = SerializerMethodField()

    class Meta:
        model = ForumPost
        fields = [
            'title',
            'pk',
            'date',
            'sub_category',
            'user',
            'post_link',
            'view_count',
            'like_count',
            'reply_count',
            'user_profile',
            'comments_link',
            'recent_reply'
        ]

    def get_recent_reply(self, obj):
        query = ForumPostComment.objects.filter(post=obj).order_by('-date').first()
        serializer = ForumLatestCommentSerializer(query)
        return serializer.data


class ForumSubCategorySerializer(ModelSerializer):
    """
        Serializer for sub category
    """

    sub_category_link = HyperlinkedIdentityField(
        view_name='forum-api:sub_category',
        lookup_field='slug'
    )

    #ForumPost = ForumPostInfoSerializer(source='sub_cat', many=True, read_only=True)
    ForumPostList = HyperlinkedIdentityField(
        view_name='forum-api:post_list',
        lookup_field='slug'
    )

    class Meta:
        model = ForumSubCategory
        fields = [
            'main',
            'category',
            'post_count',
            'reply_count',
            'like_count',
            'slug',
            'order',
            'sub_category_link',
            'ForumPostList'
            #'ForumPost'
        ]


class ForumImageSerializer(ModelSerializer):
    """
        image serializer for forum
    """

    class Meta:
        model = ForumPostImage
        fields = [
            'user',
            'image_used',
            'image'
        ]
        read_only_fields = [
            'user'
        ]


