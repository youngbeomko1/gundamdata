from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView

from forum.models import (
    ForumMainCategory,
    ForumSubCategory,
    ForumPost,
    ForumPostComment,
    ForumPostImage
)

from rest_framework.permissions import(
    IsAuthenticated,
    AllowAny,
    IsAuthenticatedOrReadOnly

)

from rest_framework.mixins import UpdateModelMixin, CreateModelMixin

from .paginations import ForumLimitOffsetPagination, ForumPageNumberPagination, ForumCommentsNumberPagination

from .serializers import (
    ForumPostSerializer,
    ForumCategorySerializer,
    ForumSubCategorySerializer,
    ForumCommentSerializer,
    ForumImageSerializer,
    ForumPostInfoSerializer,


    ForumMainPageSerializer,

    ForumRecentMainSerializer
)


def count_posts_replies(category, post):
    """
        count total number of post and replies when a reply or forum post is created
    """
    cate = category
    forum = post


    cate.post_count = ForumPost.objects.filter(sub_category=cate).count()
    cate.reply_count = ForumPostComment.objects.filter(post=forum).count()
    forum.reply_count = ForumPostComment.objects.filter(post=forum).count()

    forum.save()
    cate.save()


class ForumMainPageAPIView(ListAPIView):
    """
        Main forum page view
    """
    queryset = ForumMainCategory.objects.all()
    serializer_class = ForumMainPageSerializer
    permission_classes = [AllowAny]

"""
class ForumRecentPostAPIView(RetrieveAPIView):
    serializer_class = ForumRecentMainSerializer
    permission_class = [AllowAny]
    queryset = ForumPost.objects.all()
    lookup_field = 'pk'

    #def get_queryset(self):
     #   return ForumPost.objects.order_by('-date')[:4]
"""


class ForumRecentPostAPIView(ListAPIView):
    serializer_class = ForumRecentMainSerializer
    permission_class = [AllowAny]
    queryset = ForumMainCategory.objects.all()[:1]





class ForumCategoryAPIView(ListAPIView):
    """
        main category
    """
    queryset = ForumMainCategory.objects.all()
    serializer_class = ForumCategorySerializer
    permission_classes = [AllowAny]


class SubCategoryAPIView(RetrieveAPIView):
    """
        sub category
    """
    queryset = ForumSubCategory.objects.all()
    serializer_class = ForumSubCategorySerializer
    lookup_field = 'slug'
    permission_classes = [AllowAny]


class ForumPostListAPIView(ListAPIView):
    """
        list of forum post
    """
    serializer_class = ForumPostInfoSerializer
    permission_classes = [AllowAny]

    pagination_class = ForumPageNumberPagination

    def get_queryset(self):
        slug = self.kwargs['slug']
        sub_cat = ForumSubCategory.objects.get(slug=slug)
        return ForumPost.objects.filter(sub_category=sub_cat)




class ForumPostTempAPIView(RetrieveAPIView):
    """
        Forum post view
    """
    queryset = ForumPost.objects.all()
    serializer_class = ForumPostSerializer
    lookup_field = 'pk'
    permission_classes = [AllowAny]


class ForumPostCreateAPIView(CreateAPIView):
    """
        forum post create (used for creating forum post)
    """
    queryset = ForumPost
    serializer_class = ForumPostSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        post = serializer.save(user=self.request.user)
        print('post: ')
        print(post)
        #post = ForumPost.objects.filter(pk=pk).first()
        count_posts_replies(post.sub_category, post)


class ForumCommentCreateAPIView(CreateAPIView):
    """
        comment create
    """
    queryset = ForumPostComment
    serializer_class = ForumCommentSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
        print(serializer)
        print('serializer')


class ForumCommentListAPIView(ListAPIView, CreateModelMixin):
    """
        Comment List view and create(used for comments get and post)
    """
    #queryset = ForumPostComment
    serializer_class = ForumCommentSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    pagination_class = ForumCommentsNumberPagination

    def get_queryset(self):
        pk = self.kwargs['pk']
        return ForumPostComment.objects.filter(post=pk)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        pk = self.kwargs['pk']
        serializer.save(user=self.request.user, post=ForumPost.objects.filter(pk=pk).first())
        post = ForumPost.objects.filter(pk=pk).first()
        count_posts_replies(post.sub_category, post)


class ForumImageListAPIView(ListAPIView, CreateModelMixin):
    """
        Image upload and list view for Forum Image
    """
    serializer_class = ForumImageSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]
    pagination_class = ForumLimitOffsetPagination

    def get_queryset(self):
        username = self.kwargs['username']
        return ForumPostImage.objects.filter(user__username=username)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
