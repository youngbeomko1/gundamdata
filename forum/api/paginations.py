from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination


class ForumLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 2
    max_limit = 48


class ForumPageNumberPagination(PageNumberPagination):
    page_size = 20


class ForumCommentsNumberPagination(PageNumberPagination):
    page_size = 20