from django.conf.urls import url

from .views import (
    ForumCategoryAPIView,


    ForumPostTempAPIView,
    SubCategoryAPIView,

    ForumPostListAPIView,

    ForumPostCreateAPIView,
    ForumCommentCreateAPIView,
    ForumCommentListAPIView,

    ForumImageListAPIView,




    ForumMainPageAPIView,
    ForumRecentPostAPIView,


)

urlpatterns = [

    url(r'^$', ForumRecentPostAPIView.as_view(), name='category'),

    #url(r'^hi/(?P<pk>\d+)/$', ForumRecentPostAPIView.as_view(), name='hi'),
    url(r'^hi/$', ForumRecentPostAPIView.as_view(), name='hi'),




    url(r'^(?P<slug>\w+)/$', SubCategoryAPIView.as_view(), name='sub_category'),

    url(r'^post_list/(?P<slug>\w+)/$', ForumPostListAPIView.as_view(), name='post_list'),

    url(r'^forum_post/(?P<pk>\d+)/$', ForumPostTempAPIView.as_view(), name='forum_post'),

    url(r'^forum_post/create/$', ForumPostCreateAPIView.as_view(), name='create'),
    url(r'^forum_post/comment/create/$', ForumCommentCreateAPIView.as_view(), name='create_comment'),
    url(r'^forum_post/(?P<pk>\d+)/comments/$', ForumCommentListAPIView.as_view(), name='forum_comment'),

    url(r'^forum_post/image/(?P<username>\w+)/$', ForumImageListAPIView.as_view(), name='forum_image'),
]