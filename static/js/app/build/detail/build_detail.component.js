'use strict';

angular.module('buildDetail').
    component('buildDetail', {
        templateUrl: '/api/templates/build_detail.html',
        controller: function($scope, $cookies, $routeParams, $location, Build){

        var grade = $routeParams.grade
        var number = $routeParams.number
        var user = $routeParams.user

        var token = $cookies.get('token')
        var username = $cookies.get('username')

        $scope.edit = null

        if(username == user){
            console.log('same user')
            $scope.edit = true
        }




        function successCallback(data, response, status, config, statusText){
            var data = data;
            $scope.detail = data;
            console.log('success')
        };

        function errorCallback(response, status, config, statusText){
            console.log(response)
            console.log('error')
        };

        Build.detail({'grade': grade, 'number': number, 'user': user},successCallback, errorCallback)




        }
    })