'use strict';

angular.module('buildEdit').
    component('buildEdit', {
        templateUrl: '/api/templates/build_edit.html',
        controller: function($scope, $cookies, $routeParams, $location, Build){

        var grade = $routeParams.grade
        var number = $routeParams.number
        var user = $routeParams.user

        var token = $cookies.get('token')
        var username = $cookies.get('username')

        $scope.edit = null

        if(username == user){
            $scope.edit = true
        }




        function successCallback(data, response, status, config, statusText){
            var data = data;
            $scope.detail = data;
            console.log('success')
        };

        function errorCallback(response, status, config, statusText){
            console.log(response)
            console.log('error')
            //send to create
        };

        Build.detail({'grade': grade, 'number': number, 'user': user},successCallback, errorCallback)




        }
    })