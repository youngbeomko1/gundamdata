'use strict';

angular.module('buildCreate').
    component('buildCreate', {
        templateUrl: '/api/templates/build_create.html',
        controller: function($scope, $cookies, $routeParams, $location, Build, Account){

        var grade = $routeParams.grade
        var number = $routeParams.number

        var token = $cookies.get('token')
        var username = $cookies.get('username')

        if(!token){
            var currentPath = $location.url()
            $location.path('login').search('next', currentPath);
        }
        else{
            Account.verify({
                type:'POST',
                token: token,
            },
            function(data){
                console.log(data)
                $scope.login = true;

            },
            function(data){
                var currentPath = $location.url()
                $location.path("/login").search("next", currentPath);
            })

        }


        function successCallback(data, response, status, config, statusText){
            console.log('success')
            $location.path('/' + grade + '/' + number + '/review/edit/' + username)

        };

        function errorCallback(response, status, config, statusText){
        };

        Build.detail({'grade': grade, 'number': number, 'user': username},successCallback, errorCallback)





        $scope.new_review = {};
        $scope.new_review_errors= {};
        $scope.errors = null;
        $scope.disabled = true

        $scope.$watch('new_review.build', function(){
            if($scope.new_review.build == 1 || $scope.new_review.build ==2){
                    $scope.disabled = false;
                }
                else{
                    $scope.disabled = true;
                }

        })

        $scope.$watch(function(){
            if($scope.new_review.overall){
                $scope.new_review_errors.overall = "";
            }
            else if($scope.new_review.build){
                $scope.new_review_errors.build = "";
            }
            else if($scope.new_review.user_review){
                $scope.new_review_errors.user_review = "";
            }
            else if($scope.new_review.design_proportions){
                $scope.new_review_errors.design_proportions = "";
            }
            else if($scope.new_review.articulation){
                $scope.new_review_errors.articulation = "";
            }
            else if($scope.new_review.build_quality){
                $scope.new_review_errors.build_quality = "";
            }
            else if($scope.new_review.accessories){
                $scope.new_review_errors.accessories = "";
            }
            else if($scope.new_review.features){
                $scope.new_review_errors.features = "";
            }

        })

        $scope.create_review = function(new_review){

            var post_verify = true;

            if(!$scope.new_review.build){
                $scope.new_review_errors.build = "Build can't be empty"
                post_verify = false;
            }


            var uploaded = function(data, response, status, config, statusText){
                $scope.new_review_errors = data.data;

                var data = data;
                $location.path('/' + grade + '/' + number + '/reviews/' + data.user_profile.username);
            }
            var failed = function(data, response, status, config, statusText){
                $scope.errors = data;
                $scope.new_review_errors = data.data;
            }

            if(post_verify){
                console.log(new_review.build)
                if($scope.new_review.build == 1 || $scope.new_review.build == 2 ){

                    Build.create({
                        overall : new_review.overall,
                        build : new_review.build,
                        user_review : new_review.user_review,
                        design_proportions : new_review.design_proportions,
                        articulation : new_review.articulation,
                        build_quality : new_review.build_quality,
                        accessories : new_review.accessories,
                        features : new_review.features,
                        grade:grade,
                        number: number,

                    },
                    uploaded,
                    failed
                    )

                }
                else{
                    Build.create({
                        overall : null,
                        build : new_review.build,
                        user_review : null,
                        design_proportions : null,
                        articulation : null,
                        build_quality : null,
                        accessories : null,
                        features : null,
                        grade:grade,
                        number: number,

                    },
                    uploaded,
                    failed
                    )

                }


            }


        }


        }
    })



angular.module('buildCreate').
    component('buildEdit', {
        templateUrl: '/api/templates/build_edit.html',
        controller: function($scope, $cookies, $routeParams, $location, Build, Account){

        var grade = $routeParams.grade
        var number = $routeParams.number
        var user = $routeParams.user

        var token = $cookies.get('token')
        var username = $cookies.get('username')

        $scope.edit = null

        if(username == user){
            $scope.edit = true
        }
        else{
            $location.path('/')
        }

        if(!token){
            var currentPath = $location.url()
            $location.path('login').search('next', currentPath);
        }
        else{
            Account.verify({
                type:'POST',
                token: token,
            },
            function(data){
                console.log(data)
                $scope.login = true;

            },
            function(data){
                var currentPath = $location.url()
                $location.path("/login").search("next", currentPath);
            })

        }






        function successCallback(data, response, status, config, statusText){
            var data = data;
            $scope.detail = data;
            console.log('success')

            $scope.new_review.build = data.build
            $scope.new_review.overall = data.overall
            $scope.new_review.user_review = data.user_review
            $scope.new_review.design_proportions = data.design_proportions
            $scope.new_review.articulation = data.articulation
            $scope.new_review.build_quality = data.build_quality
            $scope.new_review.accessories = data.accessories
            $scope.new_review.features = data.features
        };

        function errorCallback(response, status, config, statusText){
            console.log(response)
            console.log('error')
            //send to create
            $location.path('/' + grade + '/' + number + '/review/create')
        };

        Build.detail({'grade': grade, 'number': number, 'user': user},successCallback, errorCallback)


        $scope.new_review = {};
        $scope.new_review_errors= {};
        $scope.disabled = false;

        $scope.$watch('new_review.build', function(){
            if($scope.new_review.build == 1 || $scope.new_review.build ==2){
                    $scope.disabled = false;
                }
                else{
                    $scope.disabled = true;
                }

        })


        $scope.$watch(function(){
            if($scope.new_review.overall){
                $scope.new_review_errors.overall = "";
            }
            else if($scope.new_review.build){
                $scope.new_review_errors.build = "";
            }
            else if($scope.new_review.user_review){
                $scope.new_review_errors.user_review = "";
            }
            else if($scope.new_review.design_proportions){
                $scope.new_review_errors.design_proportions = "";
            }
            else if($scope.new_review.articulation){
                $scope.new_review_errors.articulation = "";
            }
            else if($scope.new_review.build_quality){
                $scope.new_review_errors.build_quality = "";
            }
            else if($scope.new_review.accessories){
                $scope.new_review_errors.accessories = "";
            }
            else if($scope.new_review.features){
                $scope.new_review_errors.features = "";
            }

        })



        $scope.edit_review = function(new_review){

            var post_verify = true;

            if(!$scope.new_review.build){
                $scope.new_review_errors.build = "Build can't be empty"
                post_verify = false;
            }


            var uploaded = function(data, response, status, config, statusText){
                console.log(data)
                console.log(response);
                console.log(config);
                console.log(statusText);
                $scope.new_review_errors = data.data;

                var data = data;
                $location.path('/' + grade + '/' + number + '/reviews/' + data.user_profile.username);
            }
            var failed = function(data, response, status, config, statusText){
                $scope.errors = data;
                console.log(data);
                console.log(response);
                console.log(config);
                console.log(statusText);
                $scope.new_review_errors = data.data;
            }

            if(post_verify){
                console.log(new_review.build)
                if($scope.new_review.build == 1 || $scope.new_review.build == 2 ){

                    Build.edit({
                        overall : new_review.overall,
                        build : new_review.build,
                        user_review : new_review.user_review,
                        design_proportions : new_review.design_proportions,
                        articulation : new_review.articulation,
                        build_quality : new_review.build_quality,
                        accessories : new_review.accessories,
                        features : new_review.features,
                        grade:grade,
                        number: number,
                        username: user,

                    },
                    uploaded,
                    failed
                    )

                }
                else{
                    Build.edit({
                        overall : null,
                        build : new_review.build,
                        user_review : null,
                        design_proportions : null,
                        articulation : null,
                        build_quality : null,
                        accessories : null,
                        features : null,
                        grade:grade,
                        number: number,
                        username: user,

                    },
                    uploaded,
                    failed
                    )

                }


            }


        }






        }
    })