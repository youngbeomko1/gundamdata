'use strict';

angular.module('buildList').
    component('buildList', {
        templateUrl: '/api/templates/build_list.html',
        controller: function($scope, $cookies, $routeParams, $location, Build, GunplaData){

        var grade = $routeParams.grade
        var number = $routeParams.number

        $scope.currentPage = 1;
        $scope.perPage = 12;


        function successGunplaCallback(data, response, status, config, statusTest){
                var data = data;
                $scope.gunpla = data;
                console.log(data)
            }


        function successCallback(data, response, status, config, statusText){
            var data = data;
            $scope.list = data;
            console.log(data)
            console.log('success')
        };

        function errorCallback(response, status, config, statusText){
            console.log(response)
            console.log('error')
        };

        Build.list({'grade': grade, 'number': number},successCallback, errorCallback)
        GunplaData.detail({'grade': grade, 'number': number}, successGunplaCallback, errorCallback)



        }
    })