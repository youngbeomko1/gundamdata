'use strict';

angular.module('questionDetail').
    component('questionDetail', {
        templateUrl:'/api/templates/question_detail.html',
        controller: function($scope, $routeParams, $http, $cookies, $location, $window, Question){

        var pk = $routeParams.pk

        function successCallback(data, response, status, config, statusTest){
            var data = data;
            $scope.detail = data;
            console.log(data);
        }

        function errorCallback(response, status, config, statusTest){
            console.log(response)
        }

        Question.question_detail({'pk':pk}, successCallback, errorCallback);




        $scope.answer_text = '';
        $scope.answer_error = '';

        $scope.$watch(function(){
            if($scope.answer_text){
                $scope.answer_error = "";
            }
        })

        var token = $cookies.get('token');

        $scope.new_answer = function(){
            if(token){
                if(!$scope.answer_text){
                    $scope.answer_error = ['Enter your answer']
                }
                else{
                    Question.answer_create({
                        type:'POST',
                        answer: $scope.answer_text,
                        quest: pk,

                    },
                    function(data){
                        console.log(data);
                    },
                    function(data){
                        console.log(data);
                    }
                    )
                }

            }

        }

    }

    });