'use strict';

angular.module('questionList').
    component('questionList', {
        templateUrl:'/api/templates/question_list.html',
        controller: function($scope, $routeParams, $http, $cookies, $location, $window, Question){

        var grade = $routeParams.grade
        var number = $routeParams.number

        grade = String(grade).toUpperCase()
        $scope.grade = grade


        function successCallback(data, response, status, config, statusTest){
            var data = data;
            $scope.list = data;
            console.log(data);
        }

        function errorCallback(response, status, config, statusTest){
            console.log(response)
        }
        Question.question_list({'grade':grade, 'number':number}, successCallback, errorCallback);


        $scope.question_text = '';
        $scope.question_error = '';

        $scope.$watch(function(){
            if($scope.question_text){
                $scope.question_error = "";
            }
        })

        var token = $cookies.get('token');

        $scope.new_question = function(){
            if(token){
                if(!$scope.question_text){
                    $scope.question_error = ['Enter your question']
                }
                else{
                    Question.question_create({
                        type:'POST',
                        question: $scope.question_text,
                        kit: $scope.list.pk,

                    },
                    function(data){
                        console.log(data);
                    },
                    function(data){
                        console.log(data);
                    }
                    )
                }

            }

        }

    }



});