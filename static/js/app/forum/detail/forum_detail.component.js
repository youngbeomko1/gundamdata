'use strict';

angular.module('forumDetail').
    component('forumDetail', {
        templateUrl:"/api/templates/forum_detail.html",
        controller: function(ForumList, $scope, $http, $routeParams, $location, $sce){


            var id = $routeParams.id;
            var page = $routeParams.page;

            if (!page){
                page = '1';
            }
            $scope.page = page;
            var totalPage = null;









            $scope.post_disable = false;

            $scope.tinymceModel = '';

            $scope.tinymceOptions = {
                height: 140,
                theme : 'modern',
                skin: 'light',
                menubar: false,

                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
                ],

                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview media | forecolor backcolor emoticons | codesample',
                image_advtab: true,


            }



            console.log(page)

            $scope.new_comment = {};
            $scope.new_commentError = {};






            $scope.postComments = function(tinymceModel){
                //console.log('test')

                //console.log('Editor content:', $scope.tinymceModel);


                $scope.post_disable = true;
                var text = $scope.tinymceModel;


                if(!text){
                    $scope.new_commentError.text = 'Comment may not be empty';
                }

                if(text){

                    ForumList.postReply({
                        type: 'POST',
                        comment: text,
                        //reply: id,
                        id: id,

                    },
                    function(data){
                        //console.log('post comment')
                        //console.log(data)

                        $location.url($location.path());

                        if(page == totalPage){
                            //current page equal last page,


                            if($scope.reply.count%20 == 0){
                                //if in last page and full
                                $location.path('/forum/post/'+ $scope.detail.pk).search('page',$scope.totalPage+1)
                            }
                            else{
                                $location.path('/forum/post/'+ $scope.detail.pk+'/').search('page',$scope.totalPage)
                            }

                        }
                        else{
                            $location.path('/forum/post/'+ $scope.detail.pk).search('page',$scope.totalPage)
                        }

                        $scope.tinymceModel = '';


                    },
                    function(data){
                        //console.log('post comment fail')
                        //console.log(data)
                    })
                }

            }


            function successCallback(data, response, status, config, statusText){
                var detail = data;
                $scope.detail = detail;

                detail.post = $sce.trustAsHtml(detail.post);
            };

            function successReplyCallback(data, response, status, config, statusText){
                var reply = data;
                //console.log(data)
                $scope.reply = reply;

                //make comments html safe
                for(var i = 0; i < reply.results.length; i++){
                    reply.results[i].comment = $sce.trustAsHtml(reply.results[i].comment)
                }

                totalPage = parseInt(Math.ceil(reply.count / 20));
                $scope.totalPage = totalPage;

                //console.log('# pages');
                console.log(totalPage);
                paginate();


            };


            function errorCallback(response, status, config, statusText){
                console.log(response);
                //console.log('error')
            };



            ForumList.detail({"id": id}, successCallback, errorCallback);

            if(page){
                ForumList.reply({"id": id, 'page': page}, successReplyCallback, errorCallback);

                //console.log('page: '+page)
            }
            else{
                ForumList.reply({"id": id}, successReplyCallback, errorCallback);
                //console.log('no page')
            }


            $scope.$watch(function(){
                if($scope.new_comment.text){
                    $scope.new_commentError.text = "";
                }

            });

            console.log('hi')





            function paginate(){
                var listPagination = [];

                if (totalPage <= 5 || page <= 3){
                    //five or less page or more then 5 page with current page 3 or under
                    for (var i = 0; i < 5; i++){
                        if(i < totalPage){
                            listPagination.push({value: i+1});
                        }
                        else {
                        }
                    }
                 }
                 else if (Number(page)+2 >= totalPage){
                    // more then 5 page with last 3 page
                    for (var i = 0, a = -4; i < 5; i++, a++){
                        listPagination.push({value: a + totalPage });
                    }

                 }
                 else if(page < totalPage){
                    //more then 5 page with 4 or greater current page
                    for (var i = 0, a = -2; i < 5; i++, a++){
                        listPagination.push({value: a + Number(page) });
                    }

                 }

                else if (totalPage == null){
                }

                $scope.listPagination = listPagination
                $scope.prev = Number(page) - 1
                $scope.next = Number(page) + 1

            }







        }

});