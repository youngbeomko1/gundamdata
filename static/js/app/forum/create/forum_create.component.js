'use strict';

angular.module('forumCreate').
    component('forumCreate', {
       templateUrl: '/api/templates/forum_new_post.html',
       controller: function($scope, $cookies, $location, $http, $routeParams, ForumList, Account){

            var category = $routeParams.category

            var usernameExist = $cookies.get('username');
            var tokenExist = $cookies.get('token');
            var categoryList = {};

            $scope.post_category = null;
            $scope.new_post = {};
            $scope.new_post_errors = {};
            $scope.post_disable = false;


             $scope.tinymceOptions = {
                height: 400,
                theme : 'modern',
                skin: 'light',
                menubar: false,

                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
                ],

                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview media | forecolor backcolor emoticons | codesample',
                image_advtab: true,


            }




            $scope.$watch(function(){
                if($scope.new_post.title){
                    console.log($scope.new_post.title)
                    $scope.new_post_errors.title = "";
                }
                if($scope.new_post.category){
                    $scope.new_post_errors.category = "";
                }
                if($scope.new_post.post){
                    $scope.new_post_errors.post = "";
                }
            })


            if(tokenExist){

                var category = $routeParams.category
                if (category){
                    $scope.post_category = category;
                    $scope.new_post.category = category;
                }

                ForumList.query(function(data){
                    categoryList = data[0].main
                    $scope.ForumList = categoryList
                });

                Account.verify({
                    type:'POST',
                    token: tokenExist,
                },
                function(data){
                    console.log(data)
                    $scope.login = true;

                },
                function(data){
                    var currentPath = $location.url()
                    $location.path("/login").search("next", currentPath);
                })

            }
            else {
                var currentPath = $location.url()
                $location.path("/login").search("next", currentPath);
            }



        $scope.newForumPost = function(new_post){
                $scope.post_disable = true;

                if (!$scope.new_post.title){
                    $scope.new_post_errors.title = "Enter Title"
                    console.log($scope.new_post_errors.title )
                    $scope.post_disable = false;
                }

                if (!$scope.new_post.category){
                    $scope.new_post_errors.category = "Select Category"
                    console.log($scope.new_post_errors.category )
                    $scope.post_disable = false;
                }

                if (!$scope.new_post.post){
                    $scope.new_post_errors.post = "Content may not be empty"
                    console.log($scope.new_post_errors.post )
                    $scope.post_disable = false;
                }

                if($scope.new_post.title && $scope.new_post.category && $scope.new_post.post){
                    console.log('posting' )
                    ForumList.create({
                        type: 'POST',
                        title: new_post.title,
                        post: new_post.post,
                        sub_category: new_post.category

                    },
                    function(data){
                        //successs
                        console.log('success');
                        $location.path("/forum/post/"+ data.pk);
                    },
                    function(data){
                        //error
                        console.log('error');
                        $scope.new_post_errors = data.data
                    })
                }
            }




       }


    });