'use strict';

angular.module('forumSubCategory').
    component('forumSubCategory', {
        templateUrl:"/api/templates/forum_sub.html",
        controller: function(ForumList, $scope, $http, $routeParams){

            var slug = $routeParams.slug

            var page = $routeParams.page
            if (!page){
                page = '1';
            }
            $scope.page = page;
            var totalPage = null;

            function successCallback(data, response, status, config, statusText){
                var subCategory = data;
                $scope.subCategory = subCategory;
            };

            function successListCallback(data, response, status, config, statusText){
                var subListCategory = data;
                $scope.subListCategory = subListCategory;
                console.log(subListCategory)

                totalPage = parseInt(Math.ceil(subListCategory.count / 20));
                $scope.totalPage = totalPage;

                paginate();
            };

            function errorCallback(response, status, config, statusText){
                console.log(response)
                //console.log('error')
            };


            function paginate(){
                var listPagination = [];

                if (totalPage <= 5 || page <= 3){
                    //five or less page or more then 5 page with current page 3 or under
                    for (var i = 0; i < 5; i++){
                        if(i < totalPage){
                            listPagination.push({value: i+1});
                        }
                        else {
                        }
                    }
                 }
                 else if (Number(page)+2 >= totalPage){
                    // more then 5 page with last 3 page
                    for (var i = 0, a = -4; i < 5; i++, a++){
                        listPagination.push({value: a + totalPage });
                    }

                 }
                 else if(page < totalPage){
                    //more then 5 page with 4 or greater current page
                    for (var i = 0, a = -2; i < 5; i++, a++){
                        listPagination.push({value: a + Number(page) });
                    }

                 }

                else if (totalPage == null){
                }

                $scope.listPagination = listPagination
                $scope.prev = Number(page) - 1
                $scope.next = Number(page) + 1

            }

            ForumList.subCategory({"slug": slug}, successCallback, errorCallback)
            ForumList.subListCategory({"slug": slug, 'page':page}, successListCallback, errorCallback)


        }


});

