'use strict';

angular.module('blogList', ['core.blog', 'angularGrid', 'ngSanitize', 'infinite-scroll']);