'use strict';

angular.module('blogList').
    component('blogList', {
        templateUrl: '/api/templates/blog_list.html',
        controller: function($scope, $cookies, $routeParams, $location, $sce, angularGridInstance, Blog){

        var page = 1;
        $scope.loading = true;
        $scope.done = false

        $scope.list = []

        $scope.refresh = function(){
            angularGridInstancer.gallery.refresh();
        }

        function success(data, response, status, config, statusText){
            var raw = data;

            var data = raw.results

            var length = data.length;

            for(var i = 0; i < length; i++){
                if(data[i].type == 3){
                    data[i].video = $sce.trustAsHtml(data[i].video)
                }
            }
            $scope.list = $scope.list.concat(data);
            page = page + 1;
            $scope.loading = false;

        }

        function error(response, status, config, statusText){
            console.log(response);

            if(response.status == 404 && response.data.detail == "Invalid page."){
            }

            $scope.loading = true;
            $scope.done = true;

        }

        Blog.paginate({page:page}, success, error)


        $scope.test = function(){
            if($scope.loading == false){
                $scope.loading = true;
                Blog.paginate({page:page}, success, error)
            }



        }


        }
    })