'use strict';

angular.module('blogCreate').
    component('blogCreate', {
        templateUrl: '/api/templates/blog_create.html',
        controller: function($scope, $routeParams, $http, $cookies, $location, Blog, Account){

            $scope.new_blog = {}
            $scope.new_blog_errors = {}

            $scope.login = false;
            $scope.uploading = true;

            var prevent_answer = "Any inputs will not be saved. Are you sure you want to leave this page?"
            $scope.upload_error = "";


            var token = $cookies.get('token')
            if(!token){
                console.log('need login')
                var currentPath = $location.url()
                $location.path("/login").search("next", currentPath);
            }
            else{
                console.log('loged in')
                Account.verify({
                    type:'POST',
                    token: token,
                },
                function(data){
                    console.log(data)
                    $scope.login = true;

                },
                function(data){
                    var currentPath = $location.url()
                    $location.path("/login").search("next", currentPath);
                })
            }

            $scope.$watch(function(){
                if($scope.new_blog.title){
                    $scope.new_blog_errors.title = "";
                }
                else if($scope.new_blog.text){
                    $scope.new_blog_errors.text = "";
                }
                else if($scope.new_blog.description){
                    $scope.new_blog_errors.description = "";
                }
                else if ($scope.new_blog.thumbnail){
                    $scope.new_blog_errors.thumbnail = "";
                }
                else if ($scope.new_blog.images){
                    $scope.new_blog_errors.images
                }

            })





            $scope.create_blog = function(new_blog){

                prevent_answer = "Uploading Blog Post, leaving now might cause an error on your post."

                console.log(new_blog.thumbnail)
                console.log(new_blog.images)

                var post_verify = true;

                if(!$scope.new_blog.title){
                    $scope.new_blog_errors.title = 'Title can not be empty';
                    post_verify = false;
                }
                if(!$scope.new_blog.text){
                    $scope.new_blog_errors.text = 'Content can not be empty';
                    post_verify = false;
                }
                if(!$scope.new_blog.description){
                    $scope.new_blog.description  ='';
                }


                var uploaded = function(data){
                    $scope.upload_error = "";
                    if($scope.new_blog.images && $scope.new_blog.images.length){

                        for(var i = 0; i < $scope.new_blog.images.length; i++){
                            console.log(i)
                            console.log($scope.new_blog.images[i]);
                            var image = $scope.new_blog.images[i]
                            Blog.image(
                                {
                                    order: i,
                                    image: image,
                                    post: data.pk,
                                    user: data.user
                                },
                                function(data_p){
                                    //console.log(data_p)
                                    if(i+1 == $scope.new_blog.images.length){
                                         $scope.uploading = false;
                                         $location.path("/blog/post/" + data.pk);

                                    }
                                },
                                function(data_r){
                                    //console.log(data_r)
                                    $scope.upload_error = "We are sorry, it seems we have a issue with creating your blog post. Please try again later.";
                                    i = $scope.new_blog.images.length;
                                    Blog.remove({
                                        'pk': data.pk,
                                    },
                                    function(data){console.log(data)},
                                    function(data){console.log(data)}
                                    )
                                }
                            )
                        }

                    }
                    else{
                        $scope.uploading = false;
                        $location.path("/blog/post/" + data.pk);
                    }
                    prevent_answer = "Any inputs will not be saved. Are you sure you want to leave this page?"
                }
                var failed = function(data){
                    console.log(data)
                    $scope.new_blog_errors = data.data;
                    prevent_answer = "Any inputs will not be saved. Are you sure you want to leave this page?"
                    $scope.upload_error = "We are sorry, it seems we have a issue with creating your blog post. Please try again later.";
                }


                var image = null;

                if(post_verify){
                    if(new_blog.thumbnail){
                        console.log('passed')
                        image = new_blog.thumbnail

                        Blog.create(
                        {
                            type: 1,
                            title: new_blog.title,
                            description: new_blog.description,
                            text: new_blog.text,
                            thumbnail: image,
                            text_plain: '', video: '', video_source: '', gundam: ''
                        },
                        uploaded,
                        failed
                        )

                    }
                    else{
                        Blog.create(
                        {
                            type: 1,
                            title: new_blog.title,
                            description: new_blog.description,
                            text: new_blog.text,
                            text_plain: '', video: '', video_source: '', gundam: ''
                        },
                        uploaded,
                        failed
                        )

                    }
                }

            }

                $scope.$on('$locationChangeStart', function( event ) {
                        if($scope.login && $scope.uploading){
                            var answer = confirm(prevent_answer)
                            if (!answer) {
                                event.preventDefault();
                            }

                        }

                    })



        }

    });


angular.module('blogCreate').
    component('blogTextCreate', {
        templateUrl: '/api/templates/blog_create_text.html',
        controller: function($scope, $routeParams, $http, $cookies, $location, Blog, Account, GunplaData){

            $scope.new_blog = {}
            $scope.new_blog_errors = {}

            $scope.tinymceOptions = {
                height: 800,
                theme : 'modern',
                skin: 'light',
                menubar: false,

                plugins: [
                    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
                    'searchreplace wordcount visualblocks visualchars code fullscreen',
                    'insertdatetime media nonbreaking save table contextmenu directionality',
                    'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
                ],

                toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | preview media | forecolor backcolor emoticons | codesample',
                image_advtab: true,

                //get raw text from input
                init_instance_callback: function(editor) {
                    var textContentTrigger = function() {
                    $scope.textContent = editor.getBody().textContent;
                    $scope.$apply();
                    };

                editor.on('KeyUp', textContentTrigger);
                editor.on('ExecCommand', textContentTrigger);
                editor.on('SetContent', function(e) {
                    if(!e.initial)
                      textContentTrigger();
                    });
                }
            }

            //button disable
            $scope.button = false;

            $scope.login = false;
            $scope.uploading = true;

            var prevent_answer = "Any inputs will not be saved. Are you sure you want to leave this page?"
            $scope.upload_error = "";


            var token = $cookies.get('token')
            if(!token){
                console.log('need login')
                var currentPath = $location.url()
                $location.path("/login").search("next", currentPath);
            }
            else{
                console.log('logged in')
                Account.verify({
                    type:'POST',
                    token: token,
                },
                function(data){
                    console.log(data)
                    $scope.login = true;

                },
                function(data){
                    var currentPath = $location.url()
                    $location.path("/login").search("next", currentPath);
                })
            }

            $scope.$watch(function(){
                if($scope.new_blog.title){
                    $scope.new_blog_errors.title = "";
                }
                else if($scope.new_blog.text){
                    $scope.new_blog_errors.text = "";
                }
                else if($scope.new_blog.description){
                    $scope.new_blog_errors.description = "";
                }
                else if ($scope.new_blog.thumbnail){
                    console.log($scope.new_blog.thumbnail);



                    $scope.new_blog_errors.thumbnail = "";
                }

            })

            //BLOG gundam selection
            var grade = $routeParams.grade
            console.log(grade)
            var number = $routeParams.number
            console.log(number)

            $scope.select_grade = '';
            $scope.select_list = null;
            $scope.list_item = '';
            $scope.select_disable = true;

            var gunpla_pk = '';
            //get gundam pk from list of gunpla with grade and number
            var get_gundam_pk = function(){
                if($scope.select_grade && $scope.list_item && $scope.select_list){
                    console.log('all true')
                    var pla = $scope.select_list.filter(function(item) {return item.number == $scope.list_item && item.graded.short == $scope.select_grade});
                }
                if(pla){
                    gunpla_pk = pla[0].pk;
                    console.log(gunpla_pk)
                }
                else{
                    gunpla_pk = '';
                }
            }

            var list_success = function(data){
                $scope.select_list = data;
                $scope.select_disable = false;
                get_gundam_pk();
            }

            var list_fail = function(data){
                $console.log(data)
                $scope.select_disable = true;
                }


            if(grade && number){
                $scope.select_grade = grade;
                $scope.list_item = number;
                GunplaData.list({'grade': $scope.select_grade}, list_success, list_fail);

            }

            $scope.$watch("select_grade",function(){

                $scope.select_disable = true;
                console.log('changed')
                console.log($scope.select_grade)
                if($scope.select_grade != ''){
                    GunplaData.list({'grade': $scope.select_grade}, list_success, list_fail)
                }
                else{
                    $scope.select_list = {};
                    $scope.select_disable = true;
                }


            });






            // upload blog post
            $scope.create_blog = function(new_blog){

                $scope.button = true;
                prevent_answer = "Uploading Blog Post, leaving now might cause an error on your uploading post."

                var post_verify = true;

                if(!$scope.new_blog.title){
                    $scope.new_blog_errors.title = 'Title can not be empty';
                    post_verify = false;
                    $scope.button = false;
                }
                if(!$scope.new_blog.text){
                    $scope.new_blog_errors.text = 'Content can not be empty';
                    post_verify = false;
                    $scope.button = false;
                }
                if(!$scope.new_blog.description){
                    $scope.new_blog.description  ='';
                }


                var uploaded = function(data){
                    $scope.upload_error = "";
                    $scope.uploading = false;
                    $location.path("/blog/post/" + data.pk);

                    prevent_answer = "Any inputs will not be saved. Are you sure you want to leave this page?"
                }
                var failed = function(data){
                    console.log(data)
                    $scope.new_blog_errors = data.data;
                    prevent_answer = "Any inputs will not be saved. Are you sure you want to leave this page?"
                    $scope.upload_error = "We are sorry, it seems we have a issue with creating your blog post. Please try again later.";
                    $scope.button = false;
                }


                var image = null;

                if(post_verify){
                    get_gundam_pk();
                    if(new_blog.thumbnail){
                        image = new_blog.thumbnail

                        Blog.create(
                        {
                            type: 1,
                            title: new_blog.title,
                            description: new_blog.description,
                            text: new_blog.text,
                            text_plain: $scope.textContent,
                            thumbnail: image,
                            video: '', video_source: '', gundam: gunpla_pk
                        },
                        uploaded,
                        failed
                        )

                    }
                    else{
                        Blog.create(
                        {
                            type: 1,
                            title: new_blog.title,
                            description: new_blog.description,
                            text: new_blog.text,
                            text_plain: $scope.textContent,
                            video: '', video_source: '', gundam: gunpla_pk
                        },
                        uploaded,
                        failed
                        )

                    }
                }

            }


            $scope.$on('$locationChangeStart', function( event ) {
                if($scope.login && $scope.uploading){
                    var answer = confirm(prevent_answer)
                    if (!answer) {
                        event.preventDefault();
                    }
                }
            })


        }
    });


angular.module('blogCreate').
    component('blogImageCreate', {
        templateUrl: '/api/templates/blog_create_image.html',
        controller: function($scope, $routeParams, $http, $cookies, $location, Blog, Account, GunplaData){


            $scope.new_blog = {}
            $scope.new_blog_errors = {}

            //button disable
            $scope.button = false;

            $scope.login = false;
            $scope.uploading = true;

            var prevent_answer = "Any inputs will not be saved. Are you sure you want to leave this page?"
            $scope.upload_error = "";


            var token = $cookies.get('token')
            if(!token){
                console.log('need login')
                var currentPath = $location.url()
                $location.path("/login").search("next", currentPath);
            }
            else{
                console.log('logged in')
                Account.verify({
                    type:'POST',
                    token: token,
                },
                function(data){
                    console.log(data)
                    $scope.login = true;

                },
                function(data){
                    var currentPath = $location.url()
                    $location.path("/login").search("next", currentPath);
                })
            }

            $scope.$watch(function(){
                if($scope.new_blog.title){
                    $scope.new_blog_errors.title = "";
                }
                else if($scope.new_blog.description){
                    $scope.new_blog_errors.description = "";
                }
                else if ($scope.new_blog.images){
                    $scope.new_blog_errors.images ="";
                }

            })


            //BLOG gundam selection
            var grade = $routeParams.grade
            console.log(grade)
            var number = $routeParams.number
            console.log(number)

            $scope.select_grade = '';
            $scope.select_list = null;
            $scope.list_item = '';
            $scope.select_disable = true;

            var gunpla_pk = '';
            //get gundam pk from list of gunpla with grade and number
            var get_gundam_pk = function(){
                if($scope.select_grade && $scope.list_item && $scope.select_list){
                    console.log('all true')
                    var pla = $scope.select_list.filter(function(item) {return item.number == $scope.list_item && item.graded.short == $scope.select_grade});
                }
                if(pla){
                    gunpla_pk = pla[0].pk;
                    console.log(gunpla_pk)
                }
                else{
                    gunpla_pk = '';
                }
            }

            var list_success = function(data){
                $scope.select_list = data;
                $scope.select_disable = false;
                get_gundam_pk();
            }

            var list_fail = function(data){
                $console.log(data)
                $scope.select_disable = true;
                }


            if(grade && number){
                $scope.select_grade = grade;
                $scope.list_item = number;
                GunplaData.list({'grade': $scope.select_grade}, list_success, list_fail);

            }

            $scope.$watch("select_grade",function(){

                $scope.select_disable = true;
                console.log('changed')
                console.log($scope.select_grade)
                if($scope.select_grade != ''){
                    GunplaData.list({'grade': $scope.select_grade}, list_success, list_fail)
                }
                else{
                    $scope.select_list = {};
                    $scope.select_disable = true;
                }


            });



            $scope.create_blog = function(new_blog){

                //button disable
                $scope.button = true;

                prevent_answer = "Uploading Blog Post, leaving now might cause an error on your uploading post."

                var post_verify = true;

                if(!$scope.new_blog.title){
                    $scope.new_blog_errors.title = 'Title can not be empty';
                    post_verify = false;
                    $scope.button = false;
                }
                if(!$scope.new_blog.images){
                    $scope.new_blog_errors.images = 'Must include Image files';
                    post_verify = false;
                    $scope.button = false;
                }
                if(!$scope.new_blog.description){
                    $scope.new_blog.description  ='';
                }


                var uploaded = function(data){
                    $scope.upload_error = "";
                    if($scope.new_blog.images && $scope.new_blog.images.length){

                        for(var i = 0; i < $scope.new_blog.images.length; i++){
                            var image = $scope.new_blog.images[i]
                            Blog.image(
                                {
                                    order: i,
                                    image: image,
                                    post: data.pk,
                                    user: data.user
                                },
                                function(data_p){
                                    console.log('uploading')
                                    var x = i + 1;
                                    console.log(x)
                                    if(x >= $scope.new_blog.images.length){
                                        console.log('done')
                                         $scope.uploading = false;
                                         $location.path("/blog/post/" + data.pk);

                                    }
                                },
                                function(data_r){
                                    //console.log(data_r)
                                    $scope.upload_error = "We are sorry, it seems we have a issue with creating your blog post. Please try again later.";
                                    i = $scope.new_blog.images.length;
                                    Blog.remove({
                                        'pk': data.pk,
                                    },
                                    function(data){console.log(data)},
                                    function(data){console.log(data)}
                                    )
                                }
                            )
                        }

                    }
                    else{
                        $scope.uploading = false;
                        $location.path("/blog/post/" + data.pk);
                    }
                    prevent_answer = "Any inputs will not be saved. Are you sure you want to leave this page?"
                }
                var failed = function(data){
                    console.log(data)
                    $scope.new_blog_errors = data.data;
                    prevent_answer = "Any inputs will not be saved. Are you sure you want to leave this page?"
                    $scope.upload_error = "We are sorry, it seems we have a issue with creating your blog post. Please try again later.";
                    $scope.button = false;
                }


                var image = null;

                if(post_verify){
                    if(new_blog.images){
                        get_gundam_pk();
                        console.log('passed')
                        image = new_blog.images[0];

                        Blog.create(
                        {
                            type: 2,
                            title: new_blog.title,
                            description: new_blog.description,
                            text: '',
                            thumbnail: image,
                            text_plain: '', video: '', video_source: '', gundam: gunpla_pk
                        },
                        uploaded,
                        failed
                        )

                    }
                    else{
                        $scope.new_blog_errors.images = 'Must include Image files';
                        post_verify = false;
                        $scope.button = false;
                    }
                }



            }


            $scope.$on('$locationChangeStart', function( event ) {
                if($scope.login && $scope.uploading){
                    var answer = confirm(prevent_answer)
                    if (!answer) {
                        event.preventDefault();
                    }
                }
            })




        }
    });


angular.module('blogCreate').
    component('blogVideoCreate', {
        templateUrl: '/api/templates/blog_create_video.html',
        controller: function($scope, $routeParams, $http, $cookies, $location, $sce, Blog, Account, GunplaData){


            $scope.new_blog = {}
            $scope.new_blog_errors = {}


            //button disable
            $scope.button = false;

            $scope.login = false;
            $scope.uploading = true;

            var prevent_answer = "Any inputs will not be saved. Are you sure you want to leave this page?"
            $scope.upload_error = "";


            var token = $cookies.get('token')
            if(!token){
                console.log('need login')
                var currentPath = $location.url()
                $location.path("/login").search("next", currentPath);
            }
            else{
                console.log('logged in')
                Account.verify({
                    type:'POST',
                    token: token,
                },
                function(data){
                    console.log(data)
                    $scope.login = true;

                },
                function(data){
                    var currentPath = $location.url()
                    $location.path("/login").search("next", currentPath);
                })
            }

            $scope.$watch(function(){
                if($scope.new_blog.title){
                    $scope.new_blog_errors.title = "";
                }
                else if($scope.new_blog.video){
                    $scope.new_blog_errors.video = "";
                    $scope.preview = $sce.trustAsHtml($scope.new_blog.video)
                }
                else if($scope.new_blog.description){
                    $scope.new_blog_errors.description = "";
                }

            })


            //BLOG gundam selection
            var grade = $routeParams.grade
            console.log(grade)
            var number = $routeParams.number
            console.log(number)

            $scope.select_grade = '';
            $scope.select_list = null;
            $scope.list_item = '';
            $scope.select_disable = true;

            var gunpla_pk = '';
            //get gundam pk from list of gunpla with grade and number
            var get_gundam_pk = function(){
                if($scope.select_grade && $scope.list_item && $scope.select_list){
                    console.log('all true')
                    var pla = $scope.select_list.filter(function(item) {return item.number == $scope.list_item && item.graded.short == $scope.select_grade});
                }
                if(pla){
                    gunpla_pk = pla[0].pk;
                    console.log(gunpla_pk)
                }
                else{
                    gunpla_pk = '';
                }
            }

            var list_success = function(data){
                $scope.select_list = data;
                $scope.select_disable = false;
                get_gundam_pk();
            }

            var list_fail = function(data){
                $console.log(data)
                $scope.select_disable = true;
                }


            if(grade && number){
                $scope.select_grade = grade;
                $scope.list_item = number;
                GunplaData.list({'grade': $scope.select_grade}, list_success, list_fail);

            }

            $scope.$watch("select_grade",function(){

                $scope.select_disable = true;
                console.log('changed')
                console.log($scope.select_grade)
                if($scope.select_grade != ''){
                    GunplaData.list({'grade': $scope.select_grade}, list_success, list_fail)
                }
                else{
                    $scope.select_list = {};
                    $scope.select_disable = true;
                }


            });



            $scope.create_blog = function(new_blog){

                $scope.button = true;
                prevent_answer = "Uploading Blog Post, leaving now might cause an error on your uploading post."

                var post_verify = true;

                if(!$scope.new_blog.title){
                    $scope.new_blog_errors.title = 'Title can not be empty';
                    post_verify = false;
                    $scope.button = false;
                }
                if(!$scope.new_blog.video){
                    $scope.new_blog_errors.text = 'Video Link can not be empty';
                    post_verify = false;
                    $scope.button = false;
                }
                if(!$scope.new_blog.description){
                    $scope.new_blog.description  ='';
                }


                var uploaded = function(data){
                    $scope.upload_error = "";
                    $scope.uploading = false;
                    $location.path("/blog/post/" + data.pk);

                    prevent_answer = "Any inputs will not be saved. Are you sure you want to leave this page?"
                }
                var failed = function(data){
                    console.log(data)
                    $scope.new_blog_errors = data.data;
                    prevent_answer = "Any inputs will not be saved. Are you sure you want to leave this page?"
                    $scope.upload_error = "We are sorry, it seems we have a issue with creating your blog post. Please try again later.";
                    $scope.button = false;
                }


                var image = null;

                if(post_verify){
                    get_gundam_pk();
                    Blog.create(
                        {
                            type: 3,
                            title: new_blog.title,
                            description: new_blog.description,
                            video: new_blog.video,
                            text: '',
                            text_plain: '',  video_source: '', gundam: gunpla_pk
                        },
                        uploaded,
                        failed
                        )
                }

            }


            $scope.$on('$locationChangeStart', function( event ) {
                if($scope.login && $scope.uploading){
                    var answer = confirm(prevent_answer)
                    if (!answer) {
                        event.preventDefault();
                    }
                }
            })



        }
    });