'use strict';

angular.module('blogDetail').
    component('blogDetail', {
        templateUrl: '/api/templates/blog_detail.html',
        controller: function($scope, $cookies, $routeParams, $location, $sce, Blog){

            var pk = $routeParams.pk;
            console.log(pk);

            $scope.video = null;

            function blogError(response, status, config, statusTest){
                console.log(response);
            }

            function blogSuccess(data, response, status, config, statusTest){
                console.log(data);
                var data = data;
                $scope.blog = data;

                if($scope.blog.type == 1){
                    console.log($scope.blog.text)
                    $scope.text = $sce.trustAsHtml($scope.blog.text)
                }

                if($scope.blog.type == 3){
                    console.log($scope.blog.video)
                    $scope.video = $sce.trustAsHtml($scope.blog.video)
                }


            }
            Blog.detail({'pk': pk,}, blogSuccess, blogError)


        }


    })