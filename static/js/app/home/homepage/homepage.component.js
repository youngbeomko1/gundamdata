angular.module('homepage').
    component('homepage', {
        templateUrl:'/api/templates/homepage.html',
        controller: function($scope, $routeParams, $sce, GunplaData){

            $scope.data = []


            function success_data(data, response, status, config, statusText){
                var data = data;
                $scope.data = data;
                console.log(data)

                //safe for blog video iframe
                if(data.blog_video){
                    var videos = data.blog_video
                    var length = data.blog_video.length

                    for(var i = 0; i < length; i++){
                        data.blog_video[i].video = $sce.trustAsHtml(data.blog_video[i].video)
                    }
                }


                //blog post video html
                var length = data.new_blog.length;

                for(var i = 0; i < length; i++){

                    if($scope.data.new_blog[i].type == 3){
                        console.log($scope.data.new_blog[i].video)
                        $scope.data.new_blog[i].video = $sce.trustAsHtml($scope.data.new_blog[i].video)
                    }

                }



            }

            function error_data(response, status, config, statusText){
                console.log(response)
            }


            GunplaData.homepage({}, success_data, error_data)


        }




        });