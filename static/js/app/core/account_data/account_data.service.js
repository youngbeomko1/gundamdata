'use strict';

function transformImageRequest(data) {
    if (data === undefined)
        return data;

    var fd = new FormData();
    angular.forEach(data, function(value, key) {
      if (value instanceof FileList) {
        if (value.length == 1) {
          fd.append(key, value[0]);
        } else {
          angular.forEach(value, function(file, index) {
            fd.append(key + '_' + index, file);
          });
        }
      } else {
        fd.append(key, value);
      }
    });

    return fd;
}

angular.
    module('core.account').
        factory('Account', function($resource, $cookies, LoginRequiredInterceptor){

        var url = '/api-token-auth/';

        var login = {
            url:'/api-token-auth/',
            method: 'POST',
            params: {},
        }

        var verify = {
            url:'/api-token-verify/',
            method: 'POST',
            params: {},
        }

        var profile = {
            url:'/api/account/:username/',
            method: 'GET',
            params: {'username': '@username'},
            isArray: false,
            cache: false

        }

        var build = {
            url:'/api/rating/builduser/:username/',
            method: 'GET',
            params: {'username': '@username'},
            isArray: true,
            cache: false

        }

        var edit = {
            url: '/api/account/:username/edit/',
            method: 'PUT',
            params: {'username': '@username'},
            transformRequest: transformImageRequest,
            sendFieldsAs: 'form',
        }

        var create = {
            url: '/api/account/create/user/',
            method: 'POST',
            params: {},
        }

        var blog = {
            url: '/api/blog/user/:username/a/',
            method: 'GET',
            params: {'username': '@username'},
            isArray: true,
            cache: false
        }


        var token = $cookies.get('token')
        if(token){
            //check["headers"] = {"Authorization": "JWT " +token}
            edit['headers'] = {"Authorization": "JWT " +token,
            'Content-Type': undefined
            }
        }


        return $resource(url, {}, {
            login: login,
            verify: verify,
            profile: profile,
            create: create,
            build: build,
            edit: edit,
            blog: blog,

        })

    });