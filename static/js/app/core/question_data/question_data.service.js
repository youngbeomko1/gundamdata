
'use strict';

angular.
    module('core.question').
        factory('Question', function($resource, $cookies, LoginRequiredInterceptor){

        var url = '/api/qanda/'

        var question_list = {
            url:'/api/qanda/list/:grade/:number/',
            method: 'GET',
            params:{'grade':'@grade', 'number':'@number'},
            isArray: false,
            cache: false
        }

        var question_detail = {
            url: '/api/qanda/detail/:pk/',
            method: 'GET',
            params:{'pk':'@pk'},
            isArray: false,
            catch: false
        }


        var question_create = {
            url:'/api/qanda/question/create/',
            method: 'POST',
            params: {}
        }

        var answer_create = {
            url: '/api/qanda/answer/create/',
            method: 'POST',
            params: {}
        }

        var token = $cookies.get('token')
        if(token){
            question_create['headers'] = {"Authorization": "JWT " +token}
            answer_create['headers'] = {"Authorization": "JWT " +token}
        }


        return $resource(url, {}, {
            question_list: question_list,
            question_detail: question_detail,
            answer_create: answer_create,
            question_create: question_create,

        })


    });