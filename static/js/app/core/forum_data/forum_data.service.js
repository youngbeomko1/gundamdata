'use strict';

angular.module('core.forum_data').
    factory('ForumList', function($resource, $cookies, LoginRequiredInterceptor){
        var url = '/api/forum/'

        var query = {
            url: url,
            method: "GET",
            params: {},
            isArray: true,
            cache: false,

        }

        var subCategory = {
            url: '/api/forum/:slug/',
            method: "GET",
            params:{'slug':'@slug'},
            isArray: false,
            cache: false,

        }

        var subListCategory = {
            url: '/api/forum/post_list/:slug/',
            method: "GET",
            params:{'slug':'@slug', 'page':'@page'},
            isArray: false,
            cache: false,


        }

        var detail = {
             url: '/api/forum/forum_post/:id/',
             method: 'GET',
             params: {'id': '@id'},
             isArray: false,
             cache: false,
        }

         var create = {
            url: '/api/forum/forum_post/create/',
            method: 'POST',
            params: {},
            interceptor: {responseError: LoginRequiredInterceptor},

         }


        var reply = {
            url:'/api/forum/forum_post/:id/comments/',
            method: 'GET',
            params: {'id': '@id', 'page':'@page'},
            isArray: false,
            cache: false,
        }

        var postReply = {
            url:'/api/forum/forum_post/:id/comments/',
            method: 'POST',
            params: {'id': '@id'},
            interceptor: {responseError: LoginRequiredInterceptor},
        }


        var token = $cookies.get('token')
        if(token){
            create['headers'] = {'Authorization': 'JWT ' + token}
            postReply['headers'] = {'Authorization': 'JWT ' + token}
        }


        return $resource(url, {}, {
            query: query,
            subCategory: subCategory,
            subListCategory: subListCategory,
            detail: detail,
            create: create,
            reply: reply,
            postReply: postReply,
        })

    });