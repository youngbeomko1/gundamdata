'use strict';

angular.module('core.build_data').
    factory('Build', function($resource, $cookies, LoginRequiredInterceptor){

        var url = '/api/rating/';

        var list = {
            url: '/api/rating/list/:grade/:number/',
            method: "GET",
            params: {'grade':'@grade', 'number':'@number'},
            isArray: true,
            cache: false,

        }

        var detail = {
            url: '/api/rating/detail/:grade/:number/:user/',
            method: 'GET',
            params: {'grade':'@grade', 'number':'@number', 'user':'@user'},
            isArray: false,
            cache: false
        }

        var create = {
            url: '/api/rating/create/:grade/:number/',
            method: 'POST',
            params: {'grade':'@grade', 'number':'@number'},
            interceptor: {responseError: LoginRequiredInterceptor},

        }

        var edit = {

            url: '/api/rating/detail/:grade/:number/:username/',
            method: 'PUT',
            params: {'grade':'@grade', 'number':'@number', 'username':'@username'},
            interceptor: {responseError: LoginRequiredInterceptor},

        }



        var token = $cookies.get('token')
        if(token){
            create['headers'] = {'Authorization': 'JWT ' + token}
            edit['headers'] = {'Authorization': 'JWT ' + token}
        }



        return $resource(url, {}, {
            list: list,
            detail: detail,
            create: create,
            edit: edit
        })

    });