'use strict';

function transformImageRequest(data) {
    if (data === undefined)
        return data;

    var fd = new FormData();
    angular.forEach(data, function(value, key) {
      if (value instanceof FileList) {
        if (value.length == 1) {
          fd.append(key, value[0]);
        } else {
          angular.forEach(value, function(file, index) {
            fd.append(key + '_' + index, file);
          });
        }
      } else {
        fd.append(key, value);
      }
    });

    return fd;
}


angular.module('core.blog').
    factory('Blog', function($resource, $cookies, LoginRequiredInterceptor){

        var url = '/api/blog/';

        var list = {
            url:'/api/blog/list/',
            method: 'GET',
            params: {},
            isArray: true,
            cache: false,
        }

        var paginate = {
            url:'/api/blog/list_page/',
            method: 'GET',
            params: {},
            isArray: false,
            cache: false,
        }

        var create = {
            url: '/api/blog/create/',
            method: "POST",
            params: {},
            transformRequest: transformImageRequest,
            sendFieldsAs: 'form',
            interceptor: {responseError: LoginRequiredInterceptor},
        }

        var detail = {
            url: '/api/blog/detail/:pk/',
            method: 'GET',
            params: {'pk': '@pk'},
            isArray: false,
            cache: false
        }

        var image = {
            url: '/api/blog/upload/',
            method: 'POST',
            params: {},
            transformRequest: transformImageRequest,
            sendFieldsAs: 'form',
            interceptor: {responseError: LoginRequiredInterceptor},
        }

        var remove = {
            url: '/api/blog/remove/:pk/',
            method: 'DELETE',
            params: {'pk': '@pk'},

        }

        var token = $cookies.get('token')
        if(token){
            create['headers'] = {
            "Authorization": "JWT " +token,
            'Content-Type': undefined
            }

            image['headers'] = {
            "Authorization": "JWT " +token,
            'Content-Type': undefined
            }

            remove['headers'] = {
            "Authorization": "JWT " +token
            }
        }



        return $resource(url, {}, {
            list:list,
            create:create,
            detail:detail,
            image: image,
            remove: remove,
            paginate: paginate,
        })
    });