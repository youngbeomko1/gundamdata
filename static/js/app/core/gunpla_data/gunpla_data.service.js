'use strict';

angular.module('core.gunpla_data').
    factory('GunplaData', function($resource, $cookies, LoginRequiredInterceptor){

        var url = '/api/gundam/';

        var list = {
            url: '/api/gundam/list/:grade/',
            method: "GET",
            params: {'grade':'@grade'},
            isArray: true,
            cache: false,

        }

        var detail = {
            url: '/api/gundam/gunpla/:grade/:number/',
            method: 'GET',
            params: {'grade': '@grade'},
            isArray: false,
            cache: false
        }

        var homepage = {
            url:'/api/gundam/home/',
            method:'GET',
            params:{},
            isArray:false,
            cache:false
        }

        var blog = {
            url:'/api/blog/gunpla/:grade/:number/:type/',
            method:'GET',
            params:{'grade': '@grade', 'number': '@number', 'type': '@type'},
            isArray:true,
            cache:false
        }



        var token = $cookies.get('token')
        if(token){
            //createBuild['headers'] = {'Authorization': 'JWT ' + token}
        }



        return $resource(url, {}, {
            list: list,
            detail: detail,
            homepage: homepage,
            blog: blog,
        })

    });