'use strict';

angular.module('navBar').
    directive('navBar', function($cookies, $location){
        return{
            restrict: "E",
            templateUrl:'/api/templates/navi_bar.html',
            link: function(scope, element, attr){
                console.log('navi');

                var tokenExist = $cookies.get('token');
                var usernameExist = $cookies.get('username');
                scope.username = usernameExist

                scope.userExist = true;

                if (!tokenExist || !usernameExist){
                    $cookies.remove('token');
                    $cookies.remove('username');

                    scope.userExist = false;
                };

                console.log(scope.userExist);


            }


        }


    });


angular.module('navBar').
    directive('navBarFooter', function($cookies, $location){
        return{
            restrict: "E",
            templateUrl:'/api/templates/footer.html',
            link: function(scope, element, attr){
                console.log('footer');
            }


        }


    });