'use strict';

angular.module('pagination').
    directive('paginationDir', function(){
        return{
            restrict: 'E',
            scope: {
                currentPage: '=current',
                totalPage: '=total',
            },
            templateUrl: "/api/templates/pagination.html",
            link: function(scope, element, attr){

            }

        };



    });