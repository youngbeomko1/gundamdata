'use strict';

angular.module('gundam').config(function(
    $locationProvider,
    $resourceProvider,
    $routeProvider
){

    $locationProvider.html5Mode({
              enabled:true
            })

    $resourceProvider.defaults.stripTrailingSlashes = false;

    $routeProvider.
        //homepage
        when('/' , {
            template:"<homepage></homepage>",
        }).
        when('/about', {
            templateUrl:'/api/templates/about.html'
        }).
        when('/developer', {
            templateUrl:'/api/templates/developer.html'
        }).
        when('/login',{
            template:'<account-login></account-login>'
        }).
        when('/logout',{
            template:'<account-logout></account-logout>'
        }).
        when('/register',{
            template:'<account-create></account-create>'
        }).
        when('/profile/:username', {
            template: '<account-profile></account-profile>'
        }).
        when('/profile/user/edit', {
            template: '<account-edit></account-edit>'
        }).
        when('/profile/:username/blog/posts',{
            template: '<account-blog></account-blog>'
        }).
        when('/profile/:username/build/reviews',{
            template: '<account-review></account-review>'
        }).
        when('/profile/:username/list',{
            template: '<account-list></account-list>'
        }).
        when('/forum/post/:id', {
            template:'<forum-detail></forum-detail>'
        }).
        when('/forum/:slug', {
            template:'<forum-sub-category></forum-sub-category>'
        }).
        when('/forum', {
            template:'<forum-home></forum-home>'
        }).
        when('/forum/new/thread', {
            template:'<forum-create></forum-create>'
        }).
        when('/:grade/list/', {
            template:'<gunpla-list></gunpla-list>'
        }).
        when('/:grade/:number/detail', {
            template:'<gunpla-detail></gunpla-detail>'
        }).
        when('/:grade/:number/image', {
            template:'<gunpla-images></gunpla-images>'
        }).
        when('/:grade/:number/blog', {
            template:'<gunpla-blog></gunpla-blog>'
        }).
        when('/:grade/:number/question', {
            template:'<question-list></question-list>'
        }).
        when('/question/:pk', {
            template:'<question-detail></question-detail>'
        }).
        when('/blog/create/text', {
            template:'<blog-text-create></blog-text-create>'
        }).
        when('/blog/create/photo', {
            template:'<blog-image-create></blog-image-create>'
        }).
        when('/blog/create/video', {
            template:'<blog-video-create></blog-video-create>'
        }).
        when('/blog/post/:pk', {
            template:'<blog-detail></blog-detail>'
        }).
        when('/blog', {
            template:'<blog-list></blog-list>'
        }).
        when('/:grade/:number/reviews', {
            template:'<build-list></build-list>'
        }).
        when('/:grade/:number/review/create', {
            template:'<build-create></build-create>'
        }).
        when('/:grade/:number/review/edit/:user', {
            template:'<build-edit></build-edit>'
        }).
        when('/:grade/:number/reviews/:user', {
            template:'<build-detail></build-detail>'
        }).



        when('/test', {
            templateUrl:'/api/templates/test.html'
        }).
        when('/hg', {
            templateUrl:'/api/templates/coming_soon.html'
        }).
        when('/sd', {
            templateUrl:'/api/templates/coming_soon.html'
        }).
        when('/ng', {
            templateUrl:'/api/templates/coming_soon.html'
        }).
        otherwise({
            templateUrl: "/api/templates/error.html"
         })

});