'use strict';

angular.module('gundam', [

    //3rd party
    'ngCookies',
    'ngResource',
    'ngRoute',
    'angularUtils.directives.dirPagination',
    'ngFileUpload',


    //apps

    'homepage',

    'forumHome',
    'forumSubCategory',
    'forumDetail',
    'forumCreate',

    'accountLogin',
    'accountProfile',
    'accountCreate',
    'accountEdit',

    'gunplaList',
    'gunplaDetail',

    'questionList',
    'questionDetail',

    'blogCreate',
    'blogDetail',
    'blogList',
    'buildList',
    'buildDetail',
    'buildCreate',


   //directives
    'pagination',
    'navBar',

    //others
    'ngtimeago',
    'ui.tinymce',

]);