'use strict';

angular.module('accountEdit').
    component('accountEdit', {
        templateUrl: '/api/templates/account_edit.html',
        controller: function($scope, $cookies, $routeParams, $location, Account){


        var token = $cookies.get('token')
        var username = $cookies.get('username')


        if(!token){
            var currentPath = $location.url()
            $location.path('login').search('next', currentPath);
        }
        else{
            Account.verify({
                type:'POST',
                token: token,
            },
            function(data){
                $scope.login = true;

            },
            function(data){
                var currentPath = $location.url()
                $location.path("/login").search("next", currentPath);
            })

        }


        $scope.edit = {};
        $scope.edit_error= {};

        var current = {};
        function successCallback(data, response, status, config, statusText){
            var data = data;
            current = data
            console.log(data)
            $scope.detail = data;
            console.log('success')

            $scope.edit.profile_image = data.profile_image
            $scope.edit.bio = data.bio
            $scope.edit.level = data.level
            $scope.edit.profile_header_image = data.profile_header_image

        }

         function errorCallback(response, status, config, statusText){
            console.log(response)
            console.log('error')
         }

        Account.profile({'username':username}, successCallback, errorCallback)


        $scope.$watch(function(){
            if($scope.edit.profile_image){
                //$scope.edit_error.profile_image = '';
            }
            else if($scope.edit.level){
                $scope.edit_error.level = '';
            }
            else if($scope.edit.bio){
                $scope.edit_error.bio = '';
            }
            else if($scope.edit.profile_header_image){
                $scope.edit_error.profile_header_image = '';
            }
        })


        $scope.edit_profile = function(edit){



            var new_edit = {};
            if($scope.edit.profile_image && ($scope.edit.profile_image != current.profile_image)){
                var image = edit.profile_image
                new_edit = angular.extend(new_edit, {profile_image : image});
            }
            if($scope.edit.profile_header_image && ($scope.edit.profile_header_image != current.profile_header_image)){
                var image = edit.profile_header_image
                new_edit = angular.extend(new_edit, {profile_header_image : image});

            }

            new_edit = angular.extend(new_edit, {level : edit.level});
            new_edit = angular.extend(new_edit, {bio : edit.bio});
            new_edit = angular.extend(new_edit, {'username': username})
            console.log(new_edit);



            console.log('submit')

            var uploaded = function(data, response, status, config, statusText){
                console.log(data)
                console.log(response);
                console.log(config);
                console.log(statusText);

                $location.path('/profile/' + username);
            }

            var failed = function(data, response, status, config, statusText){
                $scope.errors = data;
                console.log(data);
                console.log(response);
                console.log(config);
                console.log(statusText);
                $scope.edit_error = data.data;
            }

            if($scope.edit){

                var profile_image_file = edit.profile_image
                var header_image_file = edit.profile_header_image

                Account.edit(new_edit, uploaded, failed)

            }

        }



    }
})