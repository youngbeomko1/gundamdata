'use strict';

angular.module('accountProfile').
    component('accountProfile', {
        templateUrl: '/api/templates/account_profile.html',
        controller: function($scope, $routeParams, $http, $cookies, $sce, Account){


            var username = $routeParams.username;

            $scope.cookie_username = $cookies.get('username')

            console.log(username)

            function successCallback(data, response, status, config, statusText){
                var data = data;
                $scope.user = data;
                console.log(data)

                var length = data.user_blog_post.length;

                for(var i = 0; i < length; i++){

                    if($scope.user.user_blog_post[i].type == 3){
                        console.log($scope.user.user_blog_post[i].video)
                        $scope.user.user_blog_post[i].video = $sce.trustAsHtml($scope.user.user_blog_post[i].video)
                    }

                }

            };

            function successBuildCallback(data, response, status, config, statusText){
                var build = data;
                $scope.build = build;
                console.log(build)

                $scope.rated = build.filter(function(build){return(build.user_review && (build.build==2 | build.build==1) ); })

                console.log($scope.rated)
            };

            function errorCallback(response, status, config, statusText){
                console.log(response);
                //console.log('error')
            };

            Account.profile({"username": username}, successCallback, errorCallback);

            Account.build({"username":username}, successBuildCallback, errorCallback)
        }


    })


angular.module('accountProfile').
    component('accountBlog', {
        templateUrl: '/api/templates/account_blog.html',
        controller: function($scope, $routeParams, $http, $cookies, $location,  $sce, $anchorScroll, Account){

            $scope.perPage = 12;
            $scope.currentPage = 1;

            var username = $routeParams.username;
            $scope.cookie_username = $cookies.get('username')

            $scope.gotoTop = function(){
                $location.hash('top');
                $anchorScroll();
            }

            function successCallback(data, response, status, config, statusText){
                var user = data;
                $scope.user = user;
                console.log(user)
            };
            function errorCallback(response, status, config, statusText){
                console.log(response);
                //console.log('error')
            };
            Account.profile({"username": username}, successCallback, errorCallback);

            function successBlogCallback(data, response, status, config, statusText){
                var data = data;
                $scope.blog = data;
                console.log(data)

                var length = data.length;

                for(var i = 0; i < length; i++){

                    if($scope.blog[i].type == 1){
                        console.log($scope.blog[i].text)
                        $scope.blog[i].text = $sce.trustAsHtml($scope.blog[i].text)
                    }

                    if($scope.blog[i].type == 3){
                        console.log($scope.blog[i].video)
                        $scope.blog[i].video = $sce.trustAsHtml($scope.blog[i].video)
                    }

                }


            };

            Account.blog({'username': username}, successBlogCallback, errorCallback);

        }
    })

angular.module('accountProfile').
    component('accountReview', {
        templateUrl: '/api/templates/account_reviews.html',
        controller: function($scope, $routeParams, $http, $cookies, Account){


            var username = $routeParams.username;

            $scope.cookie_username = $cookies.get('username')

            console.log(username)

            $scope.perPage = 12;
            $scope.currentPage = 1;

            function successCallback(data, response, status, config, statusText){
                var user = data;
                $scope.user = user;
                console.log(user)
            };

            function successBuildCallback(data, response, status, config, statusText){
                var build = data;
                $scope.rated = build.filter(function(build){return(build.user_review && (build.build==2 | build.build==1) ); })

                console.log($scope.rated)
            };

            function errorCallback(response, status, config, statusText){
                console.log(response);
                //console.log('error')
            };

            Account.profile({"username": username}, successCallback, errorCallback);

            Account.build({"username":username}, successBuildCallback, errorCallback)
        }


    })


angular.module('accountProfile').
    component('accountList', {
        templateUrl: '/api/templates/account_list.html',
        controller: function($scope, $routeParams, $http, $cookies, Account){


            var username = $routeParams.username;

            $scope.cookie_username = $cookies.get('username')

            console.log(username)

            $scope.perPage = 24;
            $scope.currentPage = 1;

            function successCallback(data, response, status, config, statusText){
                var user = data;
                $scope.user = user;
                console.log(user)
            };

            function successBuildCallback(data, response, status, config, statusText){
                var build = data;
                $scope.build = build;
            };

            function errorCallback(response, status, config, statusText){
                console.log(response);
                //console.log('error')
            };

            Account.profile({"username": username}, successCallback, errorCallback);

            Account.build({"username":username}, successBuildCallback, errorCallback)
        }


    })