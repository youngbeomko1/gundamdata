
'use strict';

angular.module('accountLogin').
    component('accountLogout', {
       templates: '',
       controller: function($cookies, $location, $route, $window, Account){

            var tokenExist = $cookies.get('token');
            var usernameExist = $cookies.get('username');

            if (tokenExist || usernameExist){
                $cookies.remove('token');
                $cookies.remove('username');
            }

            $window.location.href = '/';

       }


    });


angular.module('accountLogin').
    component('accountLogin', {
        templateUrl: '/api/templates/account_login.html',
        controller: function($scope, $routeParams, $http, $cookies, $location, $window, Account){

        $scope.user = {}
        $scope.loginError = {}

        var next = $routeParams.next
            if (!next || next == '/login' ){
                next = '/';
            }



        $scope.$watch(function(){
            if ($scope.user.password) {
                $scope.loginError.password = ""
            } else if ($scope.user.username) {
                $scope.loginError.username = ""
            }
        })


        var tokenExist = $cookies.get('token')

        $scope.userLogin = function(user){
            if(!user.username){
                $scope.loginError.username = ['Enter your username']
                }

            if (!user.password){
                $scope.loginError.password = ['Enter your password']
                }

            if(user.username && user.password){

                Account.login({
                    type: 'POST',
                    username: user.username,
                    password: user.password
                }, function(data){
                        $cookies.put('token', data.token)
                        $cookies.put('username', user.username)

                        $window.location.href = next;
                    },
                    function(data){
                        console.log(data);
                        $scope.loginError = data.data
                    }

                )

            }

        }





        }

    });