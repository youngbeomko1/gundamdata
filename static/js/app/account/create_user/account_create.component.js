'use strict';

angular.module('accountCreate').
    component('accountCreate', {
        templateUrl : '/api/templates/account_create.html',
        controller: function($scope, $cookies, $location, $routeParams, $window,  Account){

            var tokenExist = $cookies.get("token")
            if (tokenExist){
                console.log('token exists')
            }


            $scope.registerError = {}
            $scope.register = {}

            $scope.$watch(function(){
                if($scope.register.password){
                    $scope.registerError.password = ""
                }
                else if ($scope.register.username){
                    $scope.registerError.username = ""
                }
                else if ($scope.register.password2){
                    $scope.registerError.password2 = ""
                }
                else if ($scope.register.email){
                    $scope.registerError.email1 = ""
                }
                else if ($scope.register.email2){
                    $scope.registerError.email2 = ""
                }
            })

            $scope.userRegister = function(register){
                console.log(register)

                //check if empty fields
                if (!register.username){
                    $scope.registerError.username = "This Field may not be empty"
                }

                if (!register.email){
                    $scope.registerError.email1 = "This Field may not be empty"
                }

                if (!register.email2){
                    $scope.registerError.email2 = "This Field may not be empty"
                }

                if (!register.password){
                    $scope.registerError.password = "This Field may not be empty"
                }

                if (!register.password2){
                    $scope.registerError.password2 = "This Field may not be empty"
                }

                // if all fields not empty send create request
                if (register.username && register.email, register.email2, register.password, register.password2){

                    Account.create({
                        type: 'POST',
                        username: register.username,
                        email1: register.email1,
                        email2: register.email2,
                        password: register.password,
                        password2: register.password2
                    },
                    function(data){
                        // success function
                        var next = '/';
                        $window.location.href = next;
                    },
                    function(data){
                        //error function
                        console.log('ERROR')
                        $scope.registerError = data.data
                    })


                }

            }





        }


    })