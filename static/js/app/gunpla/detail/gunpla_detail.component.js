'use strict';

angular.module('gunplaDetail').
    component('gunplaDetail', {
        templateUrl:"/api/templates/gunpla_detail.html",
        controller: function(GunplaData, $scope, $cookies, $routeParams, $sce, $location){


            var grade = $routeParams.grade
            var number = $routeParams.number

            var usernameExist = $cookies.get('username');
            var tokenExist = $cookies.get('token');

            grade = String(grade).toUpperCase()
            $scope.grade = grade

            $scope.main_image = '';

            $scope.main_image_change = function(image){
                $scope.main_image = image;
            }

            $scope.image_one = null;
            $scope.image_two = null;

            function successCallback(data, response, status, config, statusTest){
                var data = data;
                $scope.gunpla = data;
                $scope.main_image = data.image;

                console.log(data)

                //load 3rd anf 4th image
                if(data.other_images){
                    var other_img = data.other_images;
                    if (other_img[0]){
                        $scope.image_one = other_img[0];
                    }
                    if (other_img[1]){
                        $scope.image_two = other_img[1];
                    }
                }

                //safe for blog video iframe
                if(data.blog_videos){
                    var videos = data.blog_videos
                    var length = data.blog_videos.length

                    for(var i = 0; i < length; i++){
                        data.blog_videos[i].video = $sce.trustAsHtml(data.blog_videos[i].video)

                    }



                    //$scope.video = $sce.trustAsHtml($scope.blog.video)
                }

            }

            function errorCallback(response, status, config, statusTest){
                console.log('error data');
                console.log(response);
            }

            GunplaData.detail({'grade': grade, 'number': number}, successCallback, errorCallback)



        }

    })



angular.module('gunplaDetail').
    component('gunplaImages', {
        templateUrl:"/api/templates/gunpla_image.html",
        controller: function(GunplaData, $scope, $cookies, $routeParams, $sce, $location){

            var grade = $routeParams.grade
            var number = $routeParams.number

            grade = String(grade).toUpperCase()
            $scope.grade = grade

            $scope.currentPage = 1;
            $scope.itemsShown = 1;
            $scope.lastPage = 1;
             $scope.perPage = 12;


            function successCallback(data, response, status, config, statusTest){
                var data = data;
                $scope.gunpla = data;
                console.log(data)
            }

            function successImageCallback(data, response, status, config, statusTest){
                var data = data;
                $scope.image = data;
                console.log(data)
            }


            function errorCallback(response, status, config, statusTest){
                console.log('error data');
                console.log(response);
            }

            GunplaData.detail({'grade': grade, 'number': number}, successCallback, errorCallback)
            GunplaData.blog({'grade': grade, 'number': number, 'type': 2}, successImageCallback, errorCallback)

        }
    })


angular.module('gunplaDetail').
    component('gunplaBlog', {
        templateUrl:"/api/templates/gunpla_blog.html",
        controller: function(GunplaData, $scope, $cookies, $routeParams, $sce, $location){

            var grade = $routeParams.grade
            var number = $routeParams.number

            grade = String(grade).toUpperCase()
            $scope.grade = grade

            $scope.currentPage = 1;
            $scope.itemsShown = 1;
            $scope.lastPage = 1;
            $scope.perPage = 12;


            function successCallback(data, response, status, config, statusTest){
                var data = data;
                $scope.gunpla = data;
                console.log(data)
            }

            function successImageCallback(data, response, status, config, statusTest){
                var data = data;
                $scope.blog_post = data;
                console.log(data)

                var length = data.length;

                for(var i = 0; i < length; i++){

                    if($scope.blog_post[i].type == 3){
                        console.log($scope.blog_post[i].video)
                        $scope.blog_post[i].video = $sce.trustAsHtml($scope.blog_post[i].video)
                    }

                }




            }


            function errorCallback(response, status, config, statusTest){
                console.log('error data');
                console.log(response);
            }
            GunplaData.detail({'grade': grade, 'number': number}, successCallback, errorCallback)
            GunplaData.blog({'grade': grade, 'number': number, 'type': 'a'}, successImageCallback, errorCallback)


        }


        })