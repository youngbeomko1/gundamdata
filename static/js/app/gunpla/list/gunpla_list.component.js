'use strict';

angular.module('gunplaList').
    component('gunplaList', {
        templateUrl:'/api/templates/gunpla_list.html',
        controller: function($scope, $routeParams, GunplaData){

            console.log('gunpla list')

            $scope.currentPage = 1;
            $scope.itemsShown = 36;
            $scope.lastPage = 1;

            var grade = $routeParams.grade

            grade = String(grade).toUpperCase()
            $scope.grade = grade


            $scope.perPage = 36;

            $scope.orderItems = '-number';
            $scope.currentOrder = 'Newest';

            $scope.changeOrderItems = function(order, current){
                $scope.orderItems = order;
                $scope.currentOrder = current;
            }


            $scope.changePageNumber = function(number){
                $scope.perPage = number;
                $scope.itemsShown = number;

            }

            function successCallback(data, response, status, config, statusTest){
                var data = data;
                $scope.gunpla = data;
                $scope.lastPage = Math.ceil(data.length/$scope.perPage);
                console.log($scope.lastPage)
                console.log(data)
            }

            function errorCallback(response, status, config, statusTest){
                console.log('error data');
                console.log(response);
            }

            GunplaData.list({'grade': grade}, successCallback, errorCallback)






        }

    })