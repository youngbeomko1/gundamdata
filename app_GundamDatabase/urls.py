"""app_GundamDatabase URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))

    curl -X POST -H "Content-Type: application/json" -d '{"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwidXNlcl9pZCI6MSwiZW1haWwiOiJhZG1pbkBhZG1pbi5jb20iLCJleHAiOjE0ODg3OTg4NjV9.9WPGop1foWn3rweVcv_FxhejT524cxCm5RpfqVS-3eI"}' http://localhost:8000/api-token-verify/


"""
from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import TemplateView

from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token

from angular_template.views import AngularTemplateView



urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),



    url(r'^api/account/', include("account.api.urls", namespace='acount-api')),
    url(r'^api/forum/', include("forum.api.urls", namespace='forum-api')),
    url(r'^api/gundam/', include("gunpla.api.urls", namespace='gunpla-api')),
    url(r'^api/rating/', include("rating.api.urls", namespace='rating-api')),
    url(r'^api/qanda/', include("qanda.api.urls", namespace='qanda-api')),
    url(r'^api/blog/', include("blog.api.urls", namespace='blog-api')),

    url(r'^api/templates/(?P<item>[A-Za-z0-9\_\-\.\/]+)\.html$',  AngularTemplateView.as_view()),

    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-verify/', verify_jwt_token),
]
"""
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
"""
urlpatterns += [
    url(r'', TemplateView.as_view(template_name='angular/home.html'))
]