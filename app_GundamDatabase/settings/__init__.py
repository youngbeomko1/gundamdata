from .base import *

live = False

try:
    from .local import *
    live = False

except:
    live = True

if live: 
    from .production import *
