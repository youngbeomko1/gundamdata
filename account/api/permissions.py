from rest_framework.permissions import BasePermission, SAFE_METHODS


class CreatorPermissions(BasePermission):
    """
        give permission to user who created the object with get(get data) put(update)
    """
    message = 'only creater can update'
    my_safe_method = ['GET', 'PUT']

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        print(obj.account)
        print(request.user)
        return obj.account == request.user


class IsOwnerPsermission(BasePermission):
    """
        give permission to owner of object
    """
    message = 'only user can access'
    my_safe_method = ['GET', 'PUT']

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return obj.user == request.user