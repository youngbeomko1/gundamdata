from django.conf.urls import url

from .views import (
    CreateUserAPIView,

    UserProfileAPIView,
    UserAllAcountsAPIView,

    UserFollowByListAPIView,
    FollowingListAPIView,
    CreateFollowAPIView,

    ProfileFollowAPIView,
    UserCheckAPIView,

    UserBuildListAPIView,
    UserProfileUpdateAPIView,
)


urlpatterns = [

        url(r'^$', UserAllAcountsAPIView.as_view(), name='userList'),
        url(r'^create/user/$', CreateUserAPIView.as_view(), name='create'),
        url(r'^(?P<account__username>\w+)/$', UserProfileAPIView.as_view(), name='userProfile'),
        url(r'^(?P<account__username>\w+)/build_list/$', UserBuildListAPIView.as_view(), name='buildList'),

        url(r'^(?P<account__username>\w+)/edit/$', UserProfileUpdateAPIView.as_view(), name='edit'),




        url(r'^(?P<account__username>\w+)/check/$', UserCheckAPIView.as_view(), name='userCheck'),

        url(r'^(?P<account__username>\w+)/followers/$', UserFollowByListAPIView.as_view(), name='userFollowers'),
        url(r'^(?P<account__username>\w+)/following/$', FollowingListAPIView.as_view(), name='userFollowing'),
        url(r'^(?P<account__username>\w+)/follow/$', CreateFollowAPIView.as_view(), name='userFollow'),

        url(r'^(?P<account__username>\w+)/followlist/$', ProfileFollowAPIView.as_view(), name='userFollowList'),
    ]