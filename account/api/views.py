from rest_framework.generics import ListAPIView, RetrieveAPIView, RetrieveUpdateAPIView, CreateAPIView, UpdateAPIView

from rest_framework.permissions import(
    IsAuthenticated,
    AllowAny
)

from rest_framework.mixins import UpdateModelMixin

from rest_framework.views import APIView

from django.contrib.auth import get_user_model

from account.models import UserAccount, FollowUser

from rating.api.serializers import BuildReviewSerializer
from rating.models import Build

from rest_framework.parsers import JSONParser, MultiPartParser, FormParser, FileUploadParser

from.permissions import CreatorPermissions
from .serializers import (

    CreateUserSerializer,

    UserProfileSerializer,
    UserPostInfoSerializer,

    FollowingSerializer,
    CreateFollowSerializer,
    FollowerSerializer,

)



User = get_user_model()

"""####################################################################"""


class CreateUserAPIView(CreateAPIView):
    """
     create user and initalize profile
    """
    serializer_class = CreateUserSerializer
    queryset = User
    permission_classes = [AllowAny]


class UserAllAcountsAPIView(ListAPIView):
    """
        list of all user
    """
    serializer_class = UserPostInfoSerializer
    queryset = UserAccount.objects.all()
    permission_classes = [AllowAny]


class UserProfileAPIView(RetrieveAPIView, UpdateModelMixin):
    """
        profile view, edit profile if owner
    """
    serializer_class = UserProfileSerializer
    queryset = UserAccount.objects.all()
    permission_classes = [CreatorPermissions]
    lookup_field = 'account__username'

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)


class UserProfileUpdateAPIView(UpdateAPIView):
    """
        update user profile
    """
    serializer_class = UserProfileSerializer
    queryset = UserAccount.objects.all()
    permission_classes = [CreatorPermissions]
    lookup_field = 'account__username'
    parser_classes = (MultiPartParser, FormParser, FileUploadParser)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

class UserCheckAPIView(RetrieveAPIView):
    """
        check if user is authenticated before post.
    """
    serializer_class = UserProfileSerializer
    queryset = UserAccount.objects.all()
    permission_classes = [CreatorPermissions, IsAuthenticated]
    lookup_field = 'account__username'

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class UserBuildListAPIView(ListAPIView):
    """
        get the list of build for a user
    """
    serializer_class = BuildReviewSerializer
    #queryset = Build.objects.all()
    permission_classes = [
        AllowAny
    ]

    def get_queryset(self):
        username = self.kwargs['account__username']
        user = User.objects.filter(username=username).first()
        print(user)
        build = Build.objects.filter(user=user)
        return build




"""####################################################################"""


class UserFollowByListAPIView(ListAPIView):
    """
     show list of all followers a user has
    """

    permission_classes = [AllowAny]
    serializer_class = FollowerSerializer

    def get_queryset(self):
        username = self.kwargs['account__username']
        return FollowUser.objects.all().filter(follow__username=username)


class FollowingListAPIView(ListAPIView):
    """
        list of all users the user is following
    """
    permission_classes = [AllowAny]
    serializer_class = FollowingSerializer

    def get_queryset(self):
        username = self.kwargs['account__username']
        return FollowUser.objects.all().filter(user__username=username)


class CreateFollowAPIView(CreateAPIView):
    """
        Follow user or unfollow user depending on if user is already following
    """
    permission_classes = [IsAuthenticated]
    serializer_class = CreateFollowSerializer


class ProfileFollowAPIView(ListAPIView):
    """
        list of followers and following for profile
    """
    permission_classes = [AllowAny]
    serializer_class = FollowerSerializer

    def get_queryset(self):
        username = self.kwargs['account__username']

        follower = [x for x in FollowUser.objects.order_by('-follow_date')
                    if x.follow.username == username]

        following = [x for x in FollowUser.objects.order_by('-follow_date')
                     if x.user.username == username]

        follower = follower[:8]
        following = following[:8]
        follow = [x for x in follower] + [y for y in following]
        return follow