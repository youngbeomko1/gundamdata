from rest_framework.serializers import (
    ModelSerializer,
    HyperlinkedIdentityField,
    SerializerMethodField,
    ValidationError,
    HyperlinkedModelSerializer,
    HyperlinkedRelatedField,
    EmailField,
    CharField,
    BooleanField,
    ReadOnlyField,
    ListSerializer
)

from rest_framework import serializers

from account.models import UserAccount, FollowUser
from django.contrib.auth import get_user_model

from rest_framework.decorators import api_view
from rest_framework.response import Response



from rating.models import Build
from blog.models import BlogPost

User = get_user_model()


class CreateUserSerializer(ModelSerializer):
    """
        serialize for user creation
    """
    email1 = EmailField(label='Email')
    email2 = EmailField(label='Confirm Email')
    password2 = CharField(label='Confirm Password')

    class Meta:
        model = User
        fields = [
            'username',
            'email1',
            'email2',
            'password',
            'password2',
        ]
    extra_kwargs = {"password": {"write_only": True}, "password2": {"write_only": True}}

    #validation

    def validate(self, data):
        return data

    def validate_email2(self, value):
        data = self.get_initial()
        email1 = data.get('email1')
        email2 = value

        check_email_used = User.objects.filter(email=email2)

        if email1 != email2:
            raise ValidationError('Email does not match')
        if check_email_used.exists():
            raise ValidationError('Email already exist')

        return value

    def validate_email1(self, value):
        data = self.get_initial()
        email1 = data.get('email1')
        check_email_used = User.objects.filter(email=email1)

        if check_email_used.exists():
            raise ValidationError('Email already exist')
        return value

    def validate_password2(self, value):
        data = self.get_initial()
        password = data.get('password')
        password2 = value

        if password != password2:
            raise ValidationError('Password does not match')
        return value

    def create(self, valid_data):
        username = valid_data['username']
        email = valid_data['email1']
        password = valid_data['password']

        #create user account
        user_create = User(
            username=username,
            email=email
        )

        user_create.set_password(password)
        user_create.save()

        #create profile for user
        user_profile = UserAccount(
            account=user_create
        )
        user_profile.save()

        return valid_data



class BlogPostUserSerializer(ModelSerializer):
    """
        simple blog posts for detail and other useage
    """


    class Meta:
        model = BlogPost
        fields = [
            'title',
            'pk',
            'date',
            'description',
            'like',
            'type',
            'thumbnail',
            'video',
            'gundam',
            'text_plain'
        ]







# used
class UserProfileSerializer(ModelSerializer):
    """
        Serializer for User Profile page.
        add all other data for profile page here
    """

    username = ReadOnlyField(source='account.username')
    levels = SerializerMethodField()

    user_blog_post = SerializerMethodField()

    followerLink = HyperlinkedIdentityField(
        view_name='acount-api:userFollowers',
        lookup_field='account__username'
    )
    followingLink = HyperlinkedIdentityField(
        view_name='acount-api:userFollowing',
        lookup_field='account__username'
    )
    follow = HyperlinkedIdentityField(
        view_name='acount-api:userFollow',
        lookup_field='account__username'
    )

    check = SerializerMethodField()

    class Meta:
        model = UserAccount
        fields = [
            'username',
            'profile_image',
            'levels',
            'level',
            'join_date',
            'bio',
            'followers',
            'following',
            'profile_header_image',

            'followerLink',
            'followingLink',
            'follow',
            'check',

            'user_blog_post'

        ]

        read_only_fields = [
            'username',
            'join_date',
            'followers',
            'following',


            'followerLink',
            'followingLink',
            'follow',

            'user_blog_post'
        ]

    def get_check(self, obj):
        user = self.context['request'].user
        if user.is_anonymous():
            return False
        follow = obj.account

        if user == follow:
            return None

        checks = FollowUser.objects.filter(user=user, follow=follow)

        if checks.exists():
            return True
        return False

    def get_levels(self, obj):
        return obj.get_level_display()

    def get_user_blog_post(self, obj):
        query = BlogPost.objects.filter(user=obj.account).order_by('-date')[:12]
        serializer = BlogPostUserSerializer(query, many=True)
        return serializer.data

    def update(self, instance, validated_data):
        try:
            profile_images = validated_data.pop('profile_image')
        except:
            profile_images = None

        try:
            profile_header_image = validated_data.pop('profile_header_image')
        except:
            profile_header_image = None

        print(instance.profile_image)
        print(profile_images)
        account = instance
        account.bio = validated_data.pop('bio')
        account.level = validated_data.pop('level')

        if profile_images:
            account.profile_image = profile_images
        if profile_header_image:
            account.profile_header_image = profile_header_image

        account.save()
        return account


# used
class UserPostInfoSerializer(ModelSerializer):
    """
        Serializer for basic user information on user created object.
        eg: blog post, fourm post, comments, etc
    """

    username = ReadOnlyField(source='account.username')
    level = SerializerMethodField()

    class Meta:
        model = UserAccount
        fields = [
            'username',
            'profile_image',
            'level',
            'bio',
            'followers',
            'following',
            'profile_header_image',
            'join_date',

        ]
        read_only_fields = [
            'username',
            'followers',
            'following'
        ]

    def get_level(self, obj):
        return obj.get_level_display()


""" follow  ####################################################################"""


# used
class FollowerSerializer(ModelSerializer):
    """
        serializer for all followers of a user
    """

    follower = UserPostInfoSerializer(source='user.account')

    user = ReadOnlyField(source='user.username')
    follow = ReadOnlyField(source='follow.username')

    class Meta:
        model = FollowUser
        fields = [
            'follower',
            'follow_date',
            'user',
            'follow'
        ]


# used
class FollowingSerializer(ModelSerializer):
    """
        Serializer for all users that user is Following
    """
    following = UserPostInfoSerializer(source='follow.account')
    user = ReadOnlyField(source='user.username')
    follow = ReadOnlyField(source='follow.username')

    class Meta:
        model = FollowUser
        fields = [
            'following',
            'follow_date',
            'user',
            'follow'
        ]


# used
class CreateFollowSerializer(ModelSerializer):
    """
        create follow if follow does not exists, if exist delete follow
    """

    class Meta:
        model = FollowUser
        fields = [
            'follow',
        ]

    def create(self, valid_data):
        user = self.context['request'].user
        follow = valid_data['follow']
        check = FollowUser.objects.filter(user=user, follow=follow)

        if check.exists():
            check.delete()

            user.account.following -= 1
            user.account.save()
            follow.account.followers -= 1
            follow.account.save()

            return valid_data

        follow_user = FollowUser(
            user=user,
            follow=follow
        )

        follow_user.save()

        user.account.following += 1
        user.account.save()
        follow.account.followers += 1
        follow.account.save()

        return valid_data