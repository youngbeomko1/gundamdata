from django.db import models
from django.contrib.auth.models import User

from PIL import Image

# Create your models here.


def profile_image_upload_location(instance, filename):
    """
    profile image location
    """
    return "img/profile_image/%s/%s" % (instance.account.username, filename)


class UserAccount(models.Model):
    """
    user account model with additional info from Django User
    """

    account = models.OneToOneField(User, on_delete=models.CASCADE, related_name='account')

    bio = models.CharField(default='', max_length=1000,  null=True, blank=True)
    location = models.CharField(default='', max_length=50, null=True, blank=True)
    join_date = models.DateTimeField(auto_now=False, auto_now_add=True)

    profile_image = models.ImageField(
        upload_to=profile_image_upload_location,
        null=False,
        blank=False,
        default='img/profile_image/default/default_image.png',
    )

    profile_header_image = models.ImageField(
        upload_to=profile_image_upload_location,
        null=True,
        blank=True
    )

    USER_LEVEL = (
        (1, 'Perfect'),
        (2, 'Master'),
        (3, 'Real'),
        (4, 'High'),
        (5, 'SD'),
        (6, 'Beginner'),
        (None, '-'),
    )

    level = models.IntegerField(null=True, default=None, choices=USER_LEVEL, blank=True)

    age = models.IntegerField(default=0, null=True, blank=True)
    followers = models.IntegerField(default=0, null=False, blank=False)
    following = models.IntegerField(default=0, null=False, blank=False)

    @property
    def account__username(self):
        return self.account.username

    def __unicode__(self):
        return self.account.username
    """
    def save(self, *args, **kwargs):
        super(UserAccount, self).save(*args, **kwargs)
        image = Image.open(self.profile_image)
        #(width, height) = image.size
        #print(image.size)
        size = (200, 200)
        image = image.resize(size, Image.ANTIALIAS)
        image.save(self.profile_image.path)
"""

class FollowUser(models.Model):
    """
    User Follow model system
    user = users im following
    follow = users following
    """

    follow_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user')
    follow = models.ForeignKey(User, on_delete=models.CASCADE, related_name='follow')

    @property
    def account__username(self):
        return self.user_me.username

    @property
    def follower__username(self):
        return self.follower.username

