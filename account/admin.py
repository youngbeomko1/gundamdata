from django.contrib import admin
from .models import UserAccount, FollowUser

# Register your models here.


class UserFollowing(admin.ModelAdmin):
    list_display = ['user', 'follow']


admin.site.register(UserAccount)
admin.site.register(FollowUser, UserFollowing)

