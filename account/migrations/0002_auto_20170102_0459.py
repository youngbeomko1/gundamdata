# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import account.models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='useraccount',
            name='height_field',
        ),
        migrations.RemoveField(
            model_name='useraccount',
            name='width_field',
        ),
        migrations.AlterField(
            model_name='useraccount',
            name='profile_image',
            field=models.ImageField(default=b'img/profile_image/default/default_image.png', null=True, upload_to=account.models.profile_image_upload_location, blank=True),
        ),
    ]
