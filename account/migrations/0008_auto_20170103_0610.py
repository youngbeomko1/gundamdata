# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0007_auto_20170102_1308'),
    ]

    operations = [
        migrations.AddField(
            model_name='useraccount',
            name='followers',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='useraccount',
            name='following',
            field=models.IntegerField(default=0),
        ),
    ]
