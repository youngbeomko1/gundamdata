# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import account.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='FollowUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('follow_date', models.DateTimeField(auto_now_add=True)),
                ('follower', models.ForeignKey(related_name='users_following_me', to=settings.AUTH_USER_MODEL)),
                ('user_me', models.ForeignKey(related_name='me_follow', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bio', models.CharField(default=b'', max_length=1000, null=True, blank=True)),
                ('location', models.CharField(default=b'', max_length=50, null=True, blank=True)),
                ('join_date', models.DateTimeField(auto_now_add=True)),
                ('profile_image', models.ImageField(default=b'profile_image/default/default_image.png', height_field=b'height_field', width_field=b'width_field', upload_to=account.models.profile_image_upload_location, blank=True, null=True)),
                ('height_field', models.IntegerField(default=0)),
                ('width_field', models.IntegerField(default=0)),
                ('profile_header_image', models.ImageField(null=True, upload_to=account.models.profile_image_upload_location, blank=True)),
                ('level', models.IntegerField(default=None, null=True, blank=True, choices=[(1, b'Perfect'), (2, b'Master'), (3, b'Real'), (4, b'High'), (5, b'SD'), (6, b'Beginner'), (None, b'-')])),
                ('account', models.OneToOneField(related_name='account', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
