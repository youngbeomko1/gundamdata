# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0005_auto_20170102_1213'),
    ]

    operations = [
        migrations.AlterField(
            model_name='followuser',
            name='follow',
            field=models.ForeignKey(related_name='follow', to='account.UserAccount'),
        ),
        migrations.AlterField(
            model_name='followuser',
            name='user',
            field=models.ForeignKey(related_name='user', to='account.UserAccount'),
        ),
    ]
