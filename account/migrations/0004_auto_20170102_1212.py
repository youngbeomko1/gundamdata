# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import account.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('account', '0003_useraccount_age'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='followuser',
            name='follower',
        ),
        migrations.RemoveField(
            model_name='followuser',
            name='user_me',
        ),
        migrations.AddField(
            model_name='followuser',
            name='follow',
            field=models.ForeignKey(related_name='follow', to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AddField(
            model_name='followuser',
            name='user',
            field=models.ForeignKey(related_name='user', to=settings.AUTH_USER_MODEL, null=True),
        ),
        migrations.AlterField(
            model_name='useraccount',
            name='profile_image',
            field=models.ImageField(default=b'img/profile_image/default/default_image.png', upload_to=account.models.profile_image_upload_location),
        ),
    ]
