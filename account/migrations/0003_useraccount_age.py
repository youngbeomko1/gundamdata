# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20170102_0459'),
    ]

    operations = [
        migrations.AddField(
            model_name='useraccount',
            name='age',
            field=models.IntegerField(default=0, null=True, blank=True),
        ),
    ]
