# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('gunpla', '0011_remove_grade_grade'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Build',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('overall', models.IntegerField(default=None, null=True, blank=True, choices=[(10, b'10'), (9, b'9'), (8, b'8'), (7, b'7'), (6, b'6'), (5, b'5'), (4, b'4'), (3, b'3'), (2, b'2'), (1, b'1'), (0, b'-')])),
                ('build', models.IntegerField(default=None, null=True, blank=True, choices=[(1, b'Built'), (2, b'Custom built'), (3, b'building'), (4, b'Own but not built'), (5, b'Wish List'), (6, b'Not Building'), (0, b'-')])),
                ('gundam', models.ForeignKey(to='gunpla.GunPla')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('review', models.TextField(null=True, blank=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('like', models.IntegerField(default=0)),
                ('dislike', models.IntegerField(default=0)),
                ('design_proportions', models.IntegerField(default=None, null=True, blank=True, choices=[(10, b'10'), (9, b'9'), (8, b'8'), (7, b'7'), (6, b'6'), (5, b'5'), (4, b'4'), (3, b'3'), (2, b'2'), (1, b'1'), (0, b'-')])),
                ('articulation', models.IntegerField(default=None, null=True, blank=True, choices=[(10, b'10'), (9, b'9'), (8, b'8'), (7, b'7'), (6, b'6'), (5, b'5'), (4, b'4'), (3, b'3'), (2, b'2'), (1, b'1'), (0, b'-')])),
                ('build_quality', models.IntegerField(default=None, null=True, blank=True, choices=[(10, b'10'), (9, b'9'), (8, b'8'), (7, b'7'), (6, b'6'), (5, b'5'), (4, b'4'), (3, b'3'), (2, b'2'), (1, b'1'), (0, b'-')])),
                ('accessories', models.IntegerField(default=None, null=True, blank=True, choices=[(10, b'10'), (9, b'9'), (8, b'8'), (7, b'7'), (6, b'6'), (5, b'5'), (4, b'4'), (3, b'3'), (2, b'2'), (1, b'1'), (0, b'-')])),
                ('features', models.IntegerField(default=None, null=True, blank=True, choices=[(10, b'10'), (9, b'9'), (8, b'8'), (7, b'7'), (6, b'6'), (5, b'5'), (4, b'4'), (3, b'3'), (2, b'2'), (1, b'1'), (0, b'-')])),
                ('gundam', models.ForeignKey(to='gunpla.GunPla')),
                ('status', models.ForeignKey(to='rating.Build')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ReviewLikes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('like_dislike', models.BooleanField()),
                ('gundam', models.ForeignKey(to='gunpla.GunPla')),
                ('review', models.ForeignKey(to='rating.Review')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
