# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rating', '0002_auto_20170323_1934'),
    ]

    operations = [
        migrations.AlterField(
            model_name='build',
            name='build',
            field=models.IntegerField(default=None, null=True, blank=True, choices=[(1, b'Built'), (2, b'Custom'), (3, b'Building'), (4, b'Bought'), (5, b'WishList'), (6, b'Skip'), (0, b'-')]),
        ),
    ]
