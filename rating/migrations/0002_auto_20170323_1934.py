# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('rating', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='build',
            name='accessories',
            field=models.IntegerField(default=None, null=True, blank=True, choices=[(10, b'10'), (9, b'9'), (8, b'8'), (7, b'7'), (6, b'6'), (5, b'5'), (4, b'4'), (3, b'3'), (2, b'2'), (1, b'1'), (0, b'-')]),
        ),
        migrations.AddField(
            model_name='build',
            name='articulation',
            field=models.IntegerField(default=None, null=True, blank=True, choices=[(10, b'10'), (9, b'9'), (8, b'8'), (7, b'7'), (6, b'6'), (5, b'5'), (4, b'4'), (3, b'3'), (2, b'2'), (1, b'1'), (0, b'-')]),
        ),
        migrations.AddField(
            model_name='build',
            name='build_quality',
            field=models.IntegerField(default=None, null=True, blank=True, choices=[(10, b'10'), (9, b'9'), (8, b'8'), (7, b'7'), (6, b'6'), (5, b'5'), (4, b'4'), (3, b'3'), (2, b'2'), (1, b'1'), (0, b'-')]),
        ),
        migrations.AddField(
            model_name='build',
            name='design_proportions',
            field=models.IntegerField(default=None, null=True, blank=True, choices=[(10, b'10'), (9, b'9'), (8, b'8'), (7, b'7'), (6, b'6'), (5, b'5'), (4, b'4'), (3, b'3'), (2, b'2'), (1, b'1'), (0, b'-')]),
        ),
        migrations.AddField(
            model_name='build',
            name='dislike',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='build',
            name='features',
            field=models.IntegerField(default=None, null=True, blank=True, choices=[(10, b'10'), (9, b'9'), (8, b'8'), (7, b'7'), (6, b'6'), (5, b'5'), (4, b'4'), (3, b'3'), (2, b'2'), (1, b'1'), (0, b'-')]),
        ),
        migrations.AddField(
            model_name='build',
            name='like',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='build',
            name='updated',
            field=models.DateTimeField(default=datetime.datetime(2017, 3, 23, 19, 34, 58, 117329, tzinfo=utc), auto_now=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='build',
            name='user_review',
            field=models.TextField(null=True, blank=True),
        ),
    ]
