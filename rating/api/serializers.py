from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
    ReadOnlyField,
    )

from rating.models import Build, Review, ReviewLikes

from django.contrib.auth import get_user_model

from account.api.serializers import UserPostInfoSerializer
from gunpla.api.serializers import GunPlaSerializer

User = get_user_model()


class BuildReviewSerializer(ModelSerializer):
    """
        new updated build serializer
    """
    user_profile = UserPostInfoSerializer(source='user.account', read_only=True)
    gundams = GunPlaSerializer(read_only=True, source='gundam')
    builds = SerializerMethodField()

    class Meta:
        model = Build
        fields = [
            'gundam',
            'user',

            'date',
            'updated',

            'overall',
            'build',
            'user_review',

            'like',
            'dislike',

            'builds',

            'design_proportions',
            'articulation',
            'build_quality',
            'accessories',
            'features',

            'user_profile',
            'gundams'


        ]
        read_only_fields = [
            'like',
            'dislike',
            'user',
            'gundam',
            'user',
            'builds'

        ]

    def get_builds(self, obj):
        return obj.get_build_display()


"""=============================================================="""
class BuildSerializer(ModelSerializer):

    builds = SerializerMethodField()
    username = ReadOnlyField(source='user.username')

    class Meta:
        model = Build
        fields = [
            'gundam',
            'username',
            'date',
            'overall',
            'builds',
            'build'

        ]
        read_only_fields = [
            'username',
            'gundam',
            'date',
            'builds'
        ]

    def get_builds(self, obj):
        return obj.get_build_display()


class BuildCreateSerializer(ModelSerializer):
    class Meta:
        model = Build
        fields = [
            'gundam',
            'user',
            'overall',
            'build'
        ]
        read_only_fields = [
            'user',
            'gundam',
        ]



class ReviewSerialier(ModelSerializer):

    builds = SerializerMethodField()

    class Meta:
        model = Review
        fields = [
            'gundam',
            'user',
            'status',
            'review',
            'date',
            'like',
            'dislike',
            'design_proportions',
            'articulation',
            'build_quality',
            'accessories',
            'features',
            'builds'

        ]

    def get_builds(self, obj):
        return obj.get_build_display()


class ReviewLikes(ModelSerializer):
    class Meta:
        fields = [
            'user',
            'review',
            'gundam',
            'like_dislike'
        ]
