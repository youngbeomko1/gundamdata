from django.conf.urls import url
from .views import (
    BuildSerialzer,
    BuildRetriveSerializer,
    BuildCreateSerializer,
    BuildReviewAPIView,
    BuildReviewDetailAPIView,
    BuildReviewCreateAPIView,

    UserBuildListAPIView

)

urlpatterns = [
    url(r'^buildlist/(?P<grade>\w+)/(?P<number>\d+)/$', BuildSerialzer.as_view(), name='buildList'),

    url(r'^list/(?P<grade>\w+)/(?P<number>\d+)/$', BuildReviewAPIView.as_view(), name='reviewList'),
    url(r'^detail/(?P<grade>\w+)/(?P<number>\d+)/(?P<user>\w+)/$', BuildReviewDetailAPIView.as_view(), name='review'),
    url(r'^create/(?P<grade>\w+)/(?P<number>\d+)/$', BuildReviewCreateAPIView.as_view(), name='new'),

    url(r'^builduser/(?P<user>\w+)/$', UserBuildListAPIView.as_view(), name='buildUser'),

    url(r'^build/(?P<grade>\w+)/(?P<number>\d+)/(?P<user>\w+)/$', BuildRetriveSerializer.as_view(), name='build'),
    url(r'^build/create/(?P<grade>\w+)/(?P<number>\d+)/$', BuildCreateSerializer.as_view(), name='create'),
    #url(r'^build/create/$', BuildCreateSerializer.as_view(), name='create'),

]