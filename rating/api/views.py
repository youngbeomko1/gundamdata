from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView

from rating.models import Build, Review, ReviewLikes
from gunpla.models import GunPla, Grade

from django.contrib.auth.models import User
from django.db.models import Avg, Sum, Count

from .serializers import BuildSerializer, ReviewSerialier, ReviewLikes, BuildCreateSerializer, BuildReviewSerializer

from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
    )

from rest_framework import serializers

from account.api.permissions import IsOwnerPsermission

from rest_framework.mixins import UpdateModelMixin, CreateModelMixin
from django.http import Http404


def build_count(BuildList, Gundam):
    gundam = Gundam
    buildlist = BuildList

    gundam.built = buildlist.filter(build=1).count()
    gundam.custom = buildlist.filter(build=2).count()
    gundam.building = buildlist.filter(build=3).count()
    gundam.purchased = buildlist.filter(build=4).count()
    gundam.wishlist = buildlist.filter(build=5).count()
    gundam.skip = buildlist.filter(build=6).count()

    gundam.overall = buildlist.exclude(overall=0).aggregate(Avg('overall')).get('overall__avg', None)
    gundam.design = buildlist.exclude(overall=0).aggregate(Avg('design_proportions')).get('design_proportions__avg', None)
    gundam.articulation = buildlist.exclude(overall=0).aggregate(Avg('articulation')).get('articulation__avg', None)
    gundam.quality = buildlist.exclude(overall=0).aggregate(Avg('build_quality')).get('build_quality__avg', None)
    gundam.accessories = buildlist.exclude(overall=0).aggregate(Avg('accessories')).get('accessories__avg', None)
    gundam.feature = buildlist.exclude(overall=0).aggregate(Avg('features')).get('features__avg', None)

    gundam.save()




class BuildReviewAPIView(ListAPIView):
    """
        view list of reviews and build
    """
    serializer_class = BuildReviewSerializer
    permission_classes = [
        AllowAny
    ]

    def get_queryset(self):
        grade = self.kwargs['grade']
        grades = Grade.objects.get(short=grade)
        number = self.kwargs['number']
        gundam = GunPla.objects.get(number=number, grade=grades)

        return Build.objects.filter(gundam=gundam)


class BuildReviewDetailAPIView(RetrieveAPIView, UpdateModelMixin):
    """
        retrive and update a review
    """
    queryset = Build.objects.all()
    serializer_class = BuildReviewSerializer
    permission_classes = [
        IsAuthenticatedOrReadOnly,
        IsOwnerPsermission
    ]

    def get_object(self):
        try:
            grade = self.kwargs['grade']
            grades = Grade.objects.get(short=grade)
            number = self.kwargs['number']
            gundam = GunPla.objects.get(number=number, grade=grades)
            username = self.kwargs['user']
            user = User.objects.get(username=username)
            return Build.objects.get(gundam=gundam, user=user)
        except:
            raise Http404("Build does not exist")

    def put(self, request, *args, **kwargs):
        grade = self.kwargs['grade']
        grades = Grade.objects.get(short=grade)
        number = self.kwargs['number']
        gundam = GunPla.objects.get(number=number, grade=grades)
        build = Build.objects.filter(gundam=gundam)

        updated = self.update(request, *args, **kwargs)
        build_count(build, gundam)

        return updated


class BuildReviewCreateAPIView(CreateAPIView):
    """
        create build review
    """
    serializer_class = BuildReviewSerializer
    permission_classes = [
        IsAuthenticatedOrReadOnly
    ]
    queryset = Build.objects.all()

    def perform_create(self, serializer):

        grade = self.kwargs['grade']
        grades = Grade.objects.get(short=grade)
        number = self.kwargs['number']
        gundam = GunPla.objects.get(number=number, grade=grades)

        if not Build.objects.filter(gundam=gundam, user=self.request.user).count():
            saved = serializer.save(user=self.request.user, gundam=gundam)
            build = Build.objects.filter(gundam=gundam)
            build_count(build, gundam)
            return saved
        else:
            item = Build.objects.get(gundam=gundam, user=self.request.user)
            if item:
                print(item.user)
                raise serializers.ValidationError("already exists")
            else:
                raise serializers.ValidationError("server error")



class UserBuildListAPIView(ListAPIView):
    """
        list of review for a single user
    """
    serializer_class = BuildReviewSerializer
    permission_classes = [
        AllowAny
    ]

    def get_queryset(self):
        user = self.kwargs['user']
        user = User.objects.filter(username=user).first()
        return Build.objects.filter(user=user)






"""======================================="""


class BuildSerialzer(ListAPIView):
    """
        view list of build objects for a gunpla
    """
    serializer_class = BuildSerializer
    permission_classes = [
        IsAuthenticatedOrReadOnly
    ]

    def get_queryset(self):
        grade = self.kwargs['grade']
        grades = Grade.objects.get(short=grade)
        number = self.kwargs['number']
        gundam = GunPla.objects.get(number=number, grade=grades)
        return Build.objects.filter(gundam=gundam)


class BuildCreateSerializer(CreateAPIView):
    """
        create build
    """
    serializer_class = BuildCreateSerializer
    permission_classes = [
        IsAuthenticatedOrReadOnly
    ]
    queryset = Build.objects.all()

    def perform_create(self, serializer):
        try:
            grade = self.kwargs['grade']
            grades = Grade.objects.get(short=grade)
            number = self.kwargs['number']
            gundam = GunPla.objects.get(number=number, grade=grades)
            if not Build.objects.filter(gundam=gundam, user=self.request.user).count():
                serializer.save(user=self.request.user, gundam=gundam)
            else:
                raise Http404("Build error: already exist")
        except:
            raise Http404("Build error: not found")

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)









class BuildRetriveSerializer(RetrieveAPIView, UpdateModelMixin):
    """
        retrive and edit build for user on a gundam
    """
    queryset = Build.objects.all()
    serializer_class = BuildSerializer
    permission_classes = [
        IsOwnerPsermission
    ]

    def get_object(self):
        try:
            #print(self.request.user)
            grade = self.kwargs['grade']
            grades = Grade.objects.get(short=grade)
            number = self.kwargs['number']
            gundam = GunPla.objects.get(number=number, grade=grades)
            username = self.kwargs['user']
            user = User.objects.get(username=username)
            return Build.objects.get(gundam=gundam, user=user)
        except:
            raise Http404("Build does not exist")

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)