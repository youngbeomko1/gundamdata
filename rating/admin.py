from django.contrib import admin
from .models import Review, Build, ReviewLikes
# Register your models here.


class reviewlist(admin.ModelAdmin):
    list_display = ['gundam', 'user']


class buildlist(admin.ModelAdmin):
    list_display = ['gundam', 'user', 'build']


class reviewlikelist(admin.ModelAdmin):
    list_display = ['review', 'user', 'gundam', 'like_dislike']

admin.site.register(Build, buildlist)
admin.site.register(Review, reviewlist)
admin.site.register(ReviewLikes, reviewlikelist)