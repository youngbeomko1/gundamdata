from django.db import models
from django.contrib.auth.models import User

from gunpla.models import GunPla

# Create your models here.


RATING = (
        (10, '10'),
        (9, '9'),
        (8, '8'),
        (7, '7'),
        (6, '6'),
        (5, '5'),
        (4, '4'),
        (3, '3'),
        (2, '2'),
        (1, '1'),
        (0, '-'),
    )


class Build(models.Model):
    """
        users build status
    """

    gundam = models.ForeignKey(GunPla, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)

    BUILDS = (
        (1, 'Built'),
        (2, 'Custom'),
        (3, 'Building'),
        (4, 'Bought'),
        (5, 'WishList'),
        (6, 'Skip'),
        (0, '-'),
    )

    overall = models.IntegerField(default=None, choices=RATING, null=True, blank=True)
    build = models.IntegerField(default=None, choices=BUILDS, null=True, blank=True)

    user_review = models.TextField(null=True, blank=True)

    like = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)

    design_proportions = models.IntegerField(default=None, choices=RATING, null=True, blank=True)
    articulation = models.IntegerField(default=None, choices=RATING, null=True, blank=True)
    build_quality = models.IntegerField(default=None, choices=RATING, null=True, blank=True)
    accessories = models.IntegerField(default=None, choices=RATING, null=True, blank=True)
    features = models.IntegerField(default=None, choices=RATING, null=True, blank=True)


class Review(models.Model):
    """
        review of gunpla via user
    """

    gundam = models.ForeignKey(GunPla, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.ForeignKey(Build, on_delete=models.CASCADE)

    review = models.TextField(null=True, blank=True)
    date = models.DateTimeField(auto_now=False, auto_now_add=True)

    like = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)

    design_proportions = models.IntegerField(default=None, choices=RATING, null=True, blank=True)
    articulation = models.IntegerField(default=None, choices=RATING, null=True, blank=True)
    build_quality = models.IntegerField(default=None, choices=RATING, null=True, blank=True)
    accessories = models.IntegerField(default=None, choices=RATING, null=True, blank=True)
    features = models.IntegerField(default=None, choices=RATING, null=True, blank=True)



class ReviewLikes(models.Model):
    """
        like system for review,
    """

    user = models.ForeignKey(User)
    review = models.ForeignKey(Review, on_delete=models.CASCADE)

    gundam = models.ForeignKey(GunPla, on_delete=models.CASCADE)

    like_dislike = models.BooleanField(blank=False)


