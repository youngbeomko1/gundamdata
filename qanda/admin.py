from django.contrib import admin
from qanda.models import Question, Answer, QuestionLike, AnswerLike
# Register your models here.


class questionlist(admin.ModelAdmin):
    list_display = ['user', 'date', 'kit', 'question']

class answerlist(admin.ModelAdmin):
    list_display = ['user', 'date', 'answer', 'quest']

admin.site.register(Question, questionlist)
admin.site.register(Answer, answerlist)