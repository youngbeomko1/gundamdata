from django.db import models
from django.contrib.auth.models import User

from gunpla.models import GunPla
# Create your models here.


class Question(models.Model):
    """
        question model
    """
    question = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)
    kit = models.ForeignKey(GunPla, on_delete=models.CASCADE)
    like = models.IntegerField(default=0)

    def __unicode__(self):
        return str("%s %s" % (self.user, self.kit))


class Answer(models.Model):
    """
        answer model
    """

    answer = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=True)
    quest = models.ForeignKey(Question, on_delete=models.CASCADE)
    like = models.IntegerField(default=0)


class QuestionLike(models.Model):
    """
        question like
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    like = models.NullBooleanField(blank=True, null=True)
    quest = models.ForeignKey(Question, on_delete=models.CASCADE)


class AnswerLike(models.Model):
    """
        answer like
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    like = models.NullBooleanField(blank=True, null=True)
    ans = models.ForeignKey(Answer, on_delete=models.CASCADE)
