from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView

from qanda.models import Question, Answer, QuestionLike, AnswerLike
from gunpla.models import GunPla, Grade


from .serializers import (
    AnswerSerializer,
    QuestionSerializer,
    GunPlaQuestionSerializer
)
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
    )


class QuestionListAPIView(RetrieveAPIView):
    """
        List of questions for a gundam
    """
    serializer_class = GunPlaQuestionSerializer
    permission_classes = [
        AllowAny
    ]
    lookup_field = 'number'

    def get_queryset(self):
        grade = self.kwargs['grade']
        grades = Grade.objects.get(short=grade)
        return GunPla.objects.filter(grade=grades)


class QuestionDetailAPIView(RetrieveAPIView):
    """
        View question and answer detail
    """
    serializer_class = QuestionSerializer
    permission_classes = [
        AllowAny
    ]
    queryset = Question.objects.all()
    lookup_field = 'pk'


class AnswerQuestionAPIView(CreateAPIView):
    """
        create answer for question
    """
    serializer_class = AnswerSerializer
    permission_classes = [
        IsAuthenticated
    ]
    queryset = Answer.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class QuestionApiView(CreateAPIView):
    """
        create new question
    """
    serializer_class = QuestionSerializer
    permission_classes = [
        IsAuthenticated
    ]
    queryset = Question.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)