from django.conf.urls import url
from .views import (
    QuestionListAPIView,
    QuestionApiView,
    AnswerQuestionAPIView,
    QuestionDetailAPIView
)

urlpatterns = [
    #url(r'^list/$', GunPlaListAPIView.as_view(), name='list'),
    #url(r'^list/(?P<grade>\w+)/$', GunPlaListAPIView.as_view(), name='list'),
    #url(r'^gunpla/(?P<grade>\w+)/(?P<number>\d+)/$', GunPlaDetailAPIView.as_view(), name='detail'),

    url(r'^list/(?P<grade>\w+)/(?P<number>\d+)/$', QuestionListAPIView.as_view(), name='list'),
    url(r'^detail/(?P<pk>\d+)/$', QuestionDetailAPIView.as_view(), name='detail'),
    url(r'^question/create/$', QuestionApiView.as_view(), name='create_question'),
    url(r'^answer/create/$', AnswerQuestionAPIView.as_view(), name='create_answer'),

]