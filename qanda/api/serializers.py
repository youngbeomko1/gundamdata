from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
    ReadOnlyField,
    )

from qanda.models import Question, Answer, QuestionLike, AnswerLike
from gunpla.models import GunPla

from django.contrib.auth import get_user_model

User = get_user_model()


class AnswerSerializer(ModelSerializer):
    """
        Answer serializer
    """
    class Meta:
        model = Answer
        fields = [
            'answer',
            'user',
            'date',
            'quest',
            'like'

        ]
        read_only_fields = [
            'user',
            'date',
            'like'
        ]



class QuestionSerializer(ModelSerializer):
    """
        Question Serializer
    """
    #answers = AnswerSerializer(many=True, read_only=True)
    answers =  SerializerMethodField()

    class Meta:
        model = Question
        fields = [
            'question',
            'user',
            'date',
            'like',
            'kit',
            'pk',
            'answers'

        ]
        read_only_fields = [
            'user',
            'date',
            'like',
            'pk',
            'answers'
        ]

    def get_answers(self, obj):
        #print(obj)
        answer = Answer.objects.filter(quest=obj)
        list = AnswerSerializer(answer, many=True).data
        return list



class GunPlaQuestionSerializer(ModelSerializer):
    """
        GunPla Question list
    """
    question_list = SerializerMethodField()

    class Meta:
        model = GunPla
        fields = [
            'number',
            'name',
            'short_name',
            'pk',
            'date',
            'price',
            'released',
            'gundam',
            'grade',
            'image',
            'image_thumb',
            'question_list'
        ]

    def get_question_list(self, obj):
        #print(obj)
        question = Question.objects.filter(kit=obj)
        list = QuestionSerializer(question, many=True).data
        return list