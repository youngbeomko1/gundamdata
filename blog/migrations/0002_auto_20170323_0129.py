# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogimage',
            name='post',
            field=models.ForeignKey(related_name='BlogPostImage', to='blog.BlogPost'),
        ),
    ]
