# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import blog.models
import django.db.models.deletion
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('gunpla', '0011_remove_grade_grade'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BlogImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('order', models.IntegerField(default=0)),
                ('image', models.ImageField(null=True, upload_to=blog.models.blog_image_upload_location, blank=True)),
                ('thumbnail', models.ImageField(null=True, upload_to=blog.models.blog_image_upload_location, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='BlogPost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=150)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('edit', models.DateTimeField(auto_now=True)),
                ('description', models.CharField(max_length=300, null=True, blank=True)),
                ('like', models.IntegerField(default=0)),
                ('type', models.IntegerField(choices=[(1, b'text'), (2, b'image'), (3, b'video')])),
                ('thumbnail', models.ImageField(null=True, upload_to=blog.models.thumbnail_image_upload_location, blank=True)),
                ('text', models.TextField(null=True, blank=True)),
                ('text_plain', models.TextField(null=True, blank=True)),
                ('video', models.CharField(max_length=300, null=True, blank=True)),
                ('video_source', models.CharField(max_length=50, null=True, blank=True)),
                ('gundam', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='gunpla.GunPla', null=True)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='blogimage',
            name='post',
            field=models.ForeignKey(to='blog.BlogPost'),
        ),
        migrations.AddField(
            model_name='blogimage',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
