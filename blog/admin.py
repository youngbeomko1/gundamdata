from django.contrib import admin
from blog.models import BlogPost, BlogImage

# Register your models here.


class blogitems(admin.ModelAdmin):
    list_display = ['title', 'user', 'date']


class blogimageitems(admin.ModelAdmin):
    list_display = [ 'user', 'date']

admin.site.register(BlogPost, blogitems)
admin.site.register(BlogImage, blogimageitems)