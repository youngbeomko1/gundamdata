from django.db import models
from django.contrib.auth.models import User

from gunpla.models import GunPla
# Create your models here.


def thumbnail_image_upload_location(instance, filename):
    """
    thumbnail image location
    """
    return "img/blog/%s/%s/%s" % (instance.user.username, instance.title, filename)


def blog_image_upload_location(instance, filename):
    """
    blog image location
    """
    return "img/blog/%s/%s/%s" % (instance.user.username, instance.post.title, filename)


POST_TYPE = (
    (1, 'text'),
    (2, 'image'),
    (3, 'video'),
)

 
class BlogPost(models.Model):
    """
        Blog Model
    """

    title = models.CharField(max_length=150, blank=False, null=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=False, auto_now_add=True)
    edit = models.DateTimeField(auto_now=True, auto_now_add=False)
    description = models.CharField(max_length=300, blank=True, null=True)
    gundam = models.ForeignKey(GunPla, blank=True, null=True, on_delete=models.SET_NULL)

    like = models.IntegerField(default=0)
    type = models.IntegerField(choices=POST_TYPE)
    thumbnail = models.ImageField(
        upload_to=thumbnail_image_upload_location,
        null=True, blank=True
    )

    text = models.TextField(blank=True, null=True)
    text_plain = models.TextField(blank=True, null=True)

    video = models.CharField(max_length=300, blank=True, null=True)
    video_source = models.CharField(max_length=50, blank=True, null=True)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ['-date']


class BlogImage(models.Model):
    """
        Blog Images
    """
    post = models.ForeignKey(BlogPost, on_delete=models.CASCADE, related_name='BlogPostImage')
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now=False, auto_now_add=True)
    order = models.IntegerField(default=0)
    image = models.ImageField(
        upload_to=blog_image_upload_location,
        null=True, blank=True
    )
    thumbnail = models.ImageField(
        upload_to=blog_image_upload_location,
        null=True, blank=True
    )