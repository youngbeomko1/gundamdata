from django.conf.urls import url
from .views import (
    BlogPostAPIView,
    BlogPostListAPIView,
    BlogCreateAPIView,
    BlogImagesAPIView,
    BlogDeleteAPIView,
    BlogPostPaginateListAPIView,
    BlogGunPlaAPIView,
    BlogUserAPIView

)
urlpatterns = [

    url(r'^list/$', BlogPostListAPIView.as_view(), name='list'),
    url(r'^create/$', BlogCreateAPIView.as_view(), name='create'),
    url(r'^upload/$', BlogImagesAPIView.as_view(), name='uploadimage'),

    url(r'^list_page/$', BlogPostPaginateListAPIView.as_view(), name='list_paginate'),

    url(r'^detail/(?P<pk>\d+)/$', BlogPostAPIView.as_view(), name='detail'),
    url(r'^remove/(?P<pk>\d+)/$', BlogDeleteAPIView.as_view(), name='remove'),

    url(r'^gunpla/(?P<grade>\w+)/(?P<number>\d+)/(?P<type>\w+)/$', BlogGunPlaAPIView.as_view(), name='gunpla'),
    url(r'^user/(?P<user>\w+)/(?P<type>\w+)/$', BlogUserAPIView.as_view(), name='user'),
]
