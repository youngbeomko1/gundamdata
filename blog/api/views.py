

from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView, DestroyAPIView

from blog.models import BlogPost, BlogImage
from .serializers import BlogImageSerializer, BlogPostSerializer

from gunpla.models import Grade, GunPla

from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
    )

from django.contrib.auth.models import User

from rest_framework.parsers import JSONParser, MultiPartParser, FormParser, FileUploadParser

from .paginations import BlogLimitOffsetPagination

from django.contrib.auth import get_user_model


User = get_user_model()


class BlogPostPaginateListAPIView(ListAPIView):
    """
        List view blog post with pagination
    """
    serializer_class = BlogPostSerializer
    permission_classes = [
        AllowAny
    ]
    queryset = BlogPost.objects.all()
    pagination_class = BlogLimitOffsetPagination



class BlogPostListAPIView(ListAPIView):
    """
        get list of blog post
    """
    serializer_class = BlogPostSerializer
    permission_classes = [
        AllowAny
    ]
    queryset = BlogPost.objects.all()


class BlogPostAPIView(RetrieveAPIView):
    """
        retrieve a blog post.
    """
    queryset = BlogPost.objects.all()
    serializer_class = BlogPostSerializer
    permission_classes = [
        AllowAny
    ]
    lookup_field = 'pk'


class BlogCreateAPIView(CreateAPIView):
    """
        create blog post
    """
    queryset = BlogPost.objects.all()
    serializer_class = BlogPostSerializer
    parser_classes = (MultiPartParser, FormParser, FileUploadParser)
    permission_classes = [
        IsAuthenticated
    ]


class BlogImagesAPIView(CreateAPIView):
    """
        upload images
    """
    queryset = BlogImage.objects.all()
    serializer_class = BlogImageSerializer
    parser_classes = (MultiPartParser, FormParser, FileUploadParser)
    permission_classes = [
        IsAuthenticated
    ]


class BlogDeleteAPIView(DestroyAPIView):
    """
        delete blog post
    """
    queryset = BlogPost.objects.all()
    serializer_class = BlogPostSerializer
    permission_classes = [
        AllowAny
    ]
    lookup_field = 'pk'


class BlogGunPlaAPIView(ListAPIView):
    """
        get blog post based gunpla and type
    """
    serializer_class = BlogPostSerializer
    permission_classes = [
        AllowAny
    ]
    queryset = BlogPost.objects.all()

    def get_queryset(self):
        grade = self.kwargs['grade']
        grades = Grade.objects.get(short=grade)
        number = self.kwargs['number']
        gundam = GunPla.objects.get(number=number, grade=grades)

        type = self.kwargs['type']
        if type.isdigit():
            return BlogPost.objects.filter(gundam=gundam, type=type)
        else:
            return BlogPost.objects.filter(gundam=gundam)


class BlogUserAPIView(ListAPIView):
    """
        get list of blog post for a user
    """
    serializer_class = BlogPostSerializer
    permission_classes = [
        AllowAny
    ]
    queryset = BlogPost.objects.all()

    def get_queryset(self):
        user = self.kwargs['user']
        user = User.objects.filter(username=user).first()

        type = self.kwargs['type']

        if type.isdigit():
            return BlogPost.objects.filter(user=user, type=type).order_by('-date')
        else:
            return BlogPost.objects.filter(user=user).order_by('-date')


