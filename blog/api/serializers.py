from rest_framework.serializers import (
    ModelSerializer,
    HyperlinkedIdentityField,
    SerializerMethodField,
    ValidationError,
    HyperlinkedModelSerializer,
    HyperlinkedRelatedField,
    EmailField,
    CharField,
    BooleanField,
    ReadOnlyField,
    ListField,
    FileField
)

from account.api.serializers import UserPostInfoSerializer
from gunpla.api.serializers import GunPlaSerializer

from blog.models import BlogPost, BlogImage

from django.contrib.auth import get_user_model


User = get_user_model()


class BlogImageSerializer(ModelSerializer):
    """
        serializer for images
    """
    class Meta:
        model = BlogImage
        fields = [
            'order',
            'image',
            'thumbnail',
            'user',
            'post'
        ]




class BlogPostSerializer(ModelSerializer):
    """
        serializer for blog post
    """
    types = SerializerMethodField()
    images = BlogImageSerializer(many=True, read_only=True, source='BlogPostImage')
    account = UserPostInfoSerializer(source='user.account', read_only=True)
    gundams = GunPlaSerializer(read_only=True, source='gundam')

    class Meta:
        model = BlogPost
        fields = [
            'title',
            'pk',
            'date',
            'edit',
            'description',
            'like',
            'types',
            'thumbnail',
            'text',
            'text_plain',
            'video',
            'video_source',
            'type',
            'gundam',
            'user',

            'gundams',
            'account',
            'images'
        ]
        read_only_fields = [
            'user'
        ]


    def get_types(self, obj):
        return obj.get_type_display()

    def create(self, validated_data):
        current_user = self.context['request'].user

        try:
            thumbnail = validated_data.pop('thumbnail')
        except:
            thumbnail = None

        blog_item = BlogPost(
            title=validated_data.pop('title'),
            description=validated_data.pop('description'),
            type=validated_data.pop('type'),
            thumbnail=thumbnail,
            text=validated_data.pop('text'),
            text_plain=validated_data.pop('text_plain'),
            video=validated_data.pop('video'),
            video_source=validated_data.pop('video_source'),
            gundam=validated_data.pop('gundam'),
            user=current_user
        )
        blog_item.save()

        return blog_item