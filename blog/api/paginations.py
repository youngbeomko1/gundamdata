from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination


class BlogLimitOffsetPagination(PageNumberPagination):
    page_size = 20
