from django.db import models
from django.contrib.auth.models import User
from django.core.files.base import ContentFile
from django.core.files.storage import default_storage as storage
from django.core.files.uploadedfile import InMemoryUploadedFile
import os
from cStringIO import StringIO
from PIL import Image

# Create your models here.


def gundam_image_upload_location(instance, filename):
    """
    gundam image location
    """
    return "img/gundam/%s/%s" % (instance.series.title, filename)


def gunpla_image_upload_location(instance, filename):
    """
        path for gunpla images
    """
    return "img/gunpla/%s/%s/%s" % (instance.grade.short, instance.number, filename)

def gunpla_image_model_upload_location(instance, filename):
    """
        path for gunpla images model
    """
    return "img/gunpla/%s/%s/%s" % (instance.gunpla.grade.short, instance.gunpla.number, filename)


class GundamUniverse(models.Model):
    """
        Gundam Universe data
    """
    universe = models.CharField(max_length=100)

    def __unicode__(self):
        return self.universe


class GundamSeries(models.Model):
    """
        Gundam Media Series
    """
    title = models.CharField(max_length=150)
    year_start = models.IntegerField(blank=True, null=True)
    year_end = models.IntegerField(blank=True, null=True)

    TYPE = (
        ('TV/OVA', 'TV/OVA'),
        ('Movies', 'Movies'),
        ('Manga', 'Manga'),
        ('Novel', 'Novel'),
        ('Game', 'Game'),
        ('Other', 'Other'),
        (None, '-')
        )

    media = models.CharField(max_length=30, choices=TYPE, null=True, blank=True)
    universe = models.ForeignKey(GundamUniverse, null=True, blank=True, related_name='gundamseries_universe')

    def __unicode__(self):
        return self.title




class GundamModel(models.Model):
    """
        contain information about gundam all gunpla shares
    """
    name = models.CharField(max_length=100)
    universe = models.ForeignKey(GundamUniverse, null=True, blank=True, related_name='gundam_universe')
    series = models.ForeignKey(GundamSeries, null=True, blank=True, related_name='gundam_series')
    image = models.ImageField(
        upload_to=gundam_image_upload_location,
        null=True,
        blank=True,
        default='img/gundam/default_gundam.jpg'

    )
    image_thumbnail = models.ImageField(
        upload_to=gundam_image_upload_location,
        null=True,
        blank=True,
        #default='img/gundam/default_gundam.jpg'
    )


    def __unicode__(self):
        return self.name

    #def save(self, *args, **kwargs):
        """
            currently only works when image for thumb is not saved, fix to work when filed already exists.
        """
        """
        super(GundamModel, self).save(*args, **kwargs)
        image = Image.open(self.image)

        width, height = image.size
        basewidth = 600

        if basewidth < width:

            width_percent = (basewidth/float(width))
            baseheight = int((float(height) * float(width_percent)))
            print(width_percent)
            print(baseheight)
            size = (basewidth, baseheight)
            print(size)
            image = image.resize(size, Image.ANTIALIAS)
            image.save(self.image.name)

        try:
            print(self.image_thumbnail.path)
        except:
            print('nope')
            f = StringIO()

            image_thumbnail = Image.open(self.image)
            image_thumbnail.thumbnail((350, 900))
            image_thumbnail.save(f, format='JPEG')
            s = f.getvalue()
            self.image_thumbnail.save(self.image.name.rsplit('/', 1)[1], ContentFile(s))
            f.close()
    """

class Grade(models.Model):
    """
        gunpla grade
    """
    #grade = models.CharField(max_length=100)
    grades = models.CharField(max_length=100)
    short = models.CharField(max_length=20)

    """
        for non hguc high grade and similar
    """
    series = models.CharField(max_length=100, null=True, blank=True)

    def __unicode__(self):
        return self.grades


class GunPla(models.Model):
    number = models.IntegerField()
    name = models.CharField(max_length=100)
    short_name = models.CharField(max_length=100)
    date = models.DateField()
    price = models.IntegerField()
    released = models.BooleanField(default=True)
    gundam = models.ForeignKey(GundamModel, null=True, blank=True, related_name='gundam_model')

    grade = models.ForeignKey(Grade, null=True, blank=True, related_name='kit_grade')


    image = models.ImageField(
        upload_to=gunpla_image_upload_location,
        null=False,
        blank=False
    )
    image_thumb = models.ImageField(
        upload_to=gunpla_image_upload_location,
        null=True,
        blank=True
    )
    box_image = models.ImageField(
        upload_to=gunpla_image_upload_location,
        null=True,
        blank=True
    )
    box_thumb = models.ImageField(
        upload_to=gunpla_image_upload_location,
        null=True,
        blank=True
    )

    count_build = models.IntegerField(default=0)
    """
        add more type of build (custiom, etc)
    """
    built = models.IntegerField(default=0)
    custom = models.IntegerField(default=0)
    building = models.IntegerField(default=0)
    purchased = models.IntegerField(default=0)
    wishlist = models.IntegerField(default=0)
    skip = models.IntegerField(default=0)

    avg_rating = models.FloatField(null=True, blank=True)
    """
        add more rating type
    """
    overall = models.FloatField(default=0, null=True)
    design = models.FloatField(default=0, null=True)
    articulation = models.FloatField(default=0, null=True)
    quality = models.FloatField(default=0, null=True)
    accessories = models.FloatField(default=0, null=True)
    feature = models.FloatField(default=0, null=True)

    class Meta:
        unique_together = ('number', 'grade')

    def __unicode__(self):
        return str("%s %s %s" % (self.grade, self.number, self.name))


class GunplaImage(models.Model):
    """
        image of gundam
    """
    gunpla = models.ForeignKey(GunPla, on_delete=models.CASCADE, related_name='GunPla_kit')
    order = models.IntegerField(null=True, blank=True)

    image = models.ImageField(
        upload_to=gunpla_image_model_upload_location,
    )
    image_thumb = models.ImageField(
        upload_to=gunpla_image_model_upload_location,
    )





