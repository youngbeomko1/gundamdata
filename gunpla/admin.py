from django.contrib import admin
from .models import GundamModel, GundamUniverse, GundamSeries, Grade, GunPla, GunplaImage
# Register your models here.

class gundamlist(admin.ModelAdmin):
    list_display = ['number', 'grade', 'name']

admin.site.register(GundamModel)
admin.site.register(GundamUniverse)
admin.site.register(GundamSeries)
admin.site.register(Grade)
admin.site.register(GunPla, gundamlist)
admin.site.register(GunplaImage)