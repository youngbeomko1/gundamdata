from django.conf.urls import url
from .views import (
    GunPlaListAPIView,
    GunPlaDetailAPIView,
    HomeDetailAPIView
)


urlpatterns = [
    #url(r'^list/$', GunPlaListAPIView.as_view(), name='list'),
    url(r'^list/(?P<grade>\w+)/$', GunPlaListAPIView.as_view(), name='list'),
    url(r'^gunpla/(?P<grade>\w+)/(?P<number>\d+)/$', GunPlaDetailAPIView.as_view(), name='detail'),

    url(r'^home/$', HomeDetailAPIView.as_view(), name='home'),

]