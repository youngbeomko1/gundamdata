from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
    ReadOnlyField,
    )

from gunpla.models import GundamModel, Grade, GunPla, GunplaImage, GundamSeries, GundamUniverse

#from rating.api.serializers import BuildReviewSerializer
from account.api.serializers import UserPostInfoSerializer
from rating.models import Build

from blog.models import BlogPost, BlogImage
from forum.models import ForumPost

from django.db.models import Q

from django.contrib.auth import get_user_model

from datetime import datetime, timedelta
from django.utils import timezone

User = get_user_model()


class UniverseSerializer(ModelSerializer):
    class Meta:
        model = GundamUniverse
        fields = [
            'universe'
        ]


class SeriesSerializer(ModelSerializer):
    class Meta:
        model = GundamSeries
        fields = [
            'title',
            'year_start',
            'year_end',
            'media',
            'universe'
        ]


class GundamSerializer(ModelSerializer):
    universes = ReadOnlyField(source='universe.universe')
    serie = SeriesSerializer(source='series', read_only=True)
    class Meta:
        model = GundamModel
        fields = [
            'name',
            'universes',
            'serie',
            'image',
            'image_thumbnail'
        ]


class GradeSerializer(ModelSerializer):
    class Meta:
        model = Grade
        fields = [
            'grades',
            'short',
            'series'
        ]


class GunplaImageSerializer(ModelSerializer):
    class Meta:
        model = GunplaImage
        fields = [
            'gunpla',
            'order',
            'image',
            'image_thumb'
        ]


class GunPlaSerializer(ModelSerializer):

    gundams = GundamSerializer(source='gundam', read_only=True)
    graded = GradeSerializer(source='grade', read_only=True)
    other_images = GunplaImageSerializer(source='GunPla_kit', read_only=True, many=True)

    class Meta:
        model = GunPla
        fields = [
            'pk',
            'number',
            'name',
            'short_name',
            'date',
            'price',
            'released',
            'image',
            'image_thumb',
            'box_image',
            'box_thumb',
            'count_build',
            'avg_rating',

            'built',
            'custom',
            'building',
            'purchased',
            'wishlist',
            'skip',

            'overall',
            'design',
            'articulation',
            'accessories',
            'feature',
            'quality',

            'gundams',
            'graded',
            'other_images'

        ]



class BuildReviewDetailSerializer(ModelSerializer):
    """
        new updated build serializer
    """
    user_profile = UserPostInfoSerializer(source='user.account', read_only=True)
    username = ReadOnlyField(source='user.username')
    builds = SerializerMethodField()

    class Meta:
        model = Build
        fields = [
            'gundam',
            'user',

            'date',
            'updated',

            'overall',
            'build',
            'builds',
            'user_review',

            'like',
            'dislike',

            'design_proportions',
            'articulation',
            'build_quality',
            'accessories',
            'features',

            'user_profile',
            'username'

        ]
        read_only_fields = [
            'like',
            'dislike',
            'user',
            'gundam',
            'user',

        ]

    def get_builds(self, obj):
        return obj.get_build_display()


class BlogImageSimpleSerializer(ModelSerializer):
    """
        serializer for images
    """
    class Meta:
        model = BlogImage
        fields = [
            'order',
            'image',
            'thumbnail',
            'user',
            'post'
        ]


class ForumPostSimpleSerializer(ModelSerializer):
    """
        serializer for forum post
    """
    class Meta:
        model = ForumPost
        fields = [
            'title',
            'pk',
            'date',
        ]


class BlogPostSimpleSerializer(ModelSerializer):
    """
        simple blog posts for detail and other useage
    """

    account = UserPostInfoSerializer(source='user.account', read_only=True)
    images = BlogImageSimpleSerializer(many=True, read_only=True, source='BlogPostImage')

    class Meta:
        model = BlogPost
        fields = [
            'title',
            'pk',
            'date',
            'description',
            'like',
            'type',
            'thumbnail',
            'video',
            'gundam',
            'account',
            'images',
            'text_plain'
        ]




class GunPlaDetailSerializer(ModelSerializer):
    """
        serializer for detail view of gundams, add other serializer later on.
    """
    gundams = GundamSerializer(source='gundam', read_only=True)
    graded = GradeSerializer(source='grade', read_only=True)
    other_images = GunplaImageSerializer(source='GunPla_kit', read_only=True, many=True)

    reviews = SerializerMethodField()
    blog_posts = SerializerMethodField()
    blog_images = SerializerMethodField()
    blog_videos = SerializerMethodField()

    class Meta:
        model = GunPla
        fields = [
            'number',
            'name',
            'short_name',
            'date',
            'price',
            'released',
            'image',
            'image_thumb',
            'box_image',
            'box_thumb',
            'count_build',
            'avg_rating',

            'built',
            'custom',
            'building',
            'purchased',
            'wishlist',
            'skip',

            'overall',
            'design',
            'articulation',
            'accessories',
            'feature',
            'quality',

            'gundams',
            'graded',
            'other_images',

            'reviews',
            'blog_posts',
            'blog_images',
            'blog_videos',

        ]

    def get_reviews(self, obj):
        """
            get reviews for gundam detail
        """
        query = Build.objects.filter(gundam=obj)[:4]
        serializer = BuildReviewDetailSerializer(query, many=True)

        return serializer.data

    def get_blog_posts(self, obj):
        """

        """
        query = BlogPost.objects.filter(gundam=obj, type=1)
        serializer = BlogPostSimpleSerializer(query, many=True)

        return serializer.data

    def get_blog_videos(self, obj):
        query = BlogPost.objects.filter(gundam=obj, type=3)
        serializer = BlogPostSimpleSerializer(query, many=True)
        return serializer.data

    def get_blog_images(self, obj):
        """

        """
        query = BlogPost.objects.filter(gundam=obj, type=2)
        serializer = BlogPostSimpleSerializer(query, many=True)
        return serializer.data



class HomeDetailSerializer(ModelSerializer):
    """
        home detail serializer
    """

    tri = SerializerMethodField()
    photo = SerializerMethodField()
    blog_video = SerializerMethodField()
    new_kit = SerializerMethodField()
    new_blog = SerializerMethodField()
    rating_kit = SerializerMethodField()
    announcement = SerializerMethodField()

    class Meta:
        model = GunPla
        fields = [
            'pk',
            'name',
            'number',

            'tri',
            'photo',
            'new_kit',
            'new_blog',
            'rating_kit',
            'blog_video',
            'announcement'
        ]

    def get_tri(self, obj):
        """
            get main page tri gunpla
        """

        query = GunPla.objects.filter(Q(grade__short='MG') |
                                      Q(grade__short='RG') |
                                      Q(grade__short='PG') |
                                      Q(grade__short='RE')).exclude(image__isnull=True).order_by('-date')[:3]
        serializer = GunPlaSerializer(query, many=True)
        return serializer.data

    def get_photo(self, obj):
        """
            get images of recent blog post
        """
        query = BlogPost.objects.filter(type=2).exclude(Q(thumbnail__isnull=True) | Q(thumbnail='')).order_by('-date')[:8]
        serializer = BlogPostSimpleSerializer(query, many=True)
        return serializer.data

    def get_new_kit(self, obj):
        """
            list of new kits
        """

        query = GunPla.objects.order_by('-date')[:8]
        serializer = GunPlaSerializer(query, many=True)
        return serializer.data

    def get_new_blog(self, obj):
        """
            list of new blog post
        """

        query = BlogPost.objects.filter(date__gte=timezone.now()-timedelta(days=14)).order_by('-like', '-date')[:24]
        if query.count() < 24:
            query = BlogPost.objects.filter(date__gte=timezone.now()-timedelta(days=60)).order_by('-like', '-date')[:24]

        serializer = BlogPostSimpleSerializer(query, many=True)
        return serializer.data

    def get_rating_kit(self, obj):
        """
            get list of most popular kits
        """

        query = GunPla.objects.order_by('-overall')[:10]
        serializer = GunPlaSerializer(query, many=True)
        return serializer.data

    def get_blog_video(self,obj):
        """
            get videos blog post
        """

        query = BlogPost.objects.filter(type=3, date__gte=timezone.now()-timedelta(days=14)).exclude(video__isnull=True).order_by('-like', '-date')[:6]
        if query.count() < 24:
            query = BlogPost.objects.filter(type=3, date__gte=timezone.now()-timedelta(days=60)).exclude(video__isnull=True).order_by('-like', '-date')[:6]
        serializer = BlogPostSimpleSerializer(query, many=True)
        return serializer.data

    def get_announcement(self, obj):
        """
            get annoucment and news from forum
        """
        query = ForumPost.objects.filter(user__username='admin', sub_category__category='Announcement').order_by('-date')[:10]
        serializer = ForumPostSimpleSerializer(query, many=True)
        return serializer.data
