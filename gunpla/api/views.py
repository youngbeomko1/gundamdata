from rest_framework.generics import ListAPIView, RetrieveAPIView, CreateAPIView

from gunpla.models import GundamModel, Grade, GunPla, GunplaImage, GundamSeries, GundamUniverse

from .serializers import (
    UniverseSerializer,
    SeriesSerializer,
    GundamSerializer,
    GradeSerializer,
    GunplaImageSerializer,
    GunPlaSerializer,
    GunPlaDetailSerializer,
    HomeDetailSerializer
    )

from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
    )

from rest_framework.response import Response


class GunPlaListAPIView(ListAPIView):
    """
        list of all gunpla
    """
    #queryset = GunPla.objects.all()
    serializer_class = GunPlaSerializer
    permission_classes = [
        AllowAny
    ]

    def get_queryset(self):
        grade = self.kwargs['grade']
        grades = Grade.objects.get(short=grade)
        return GunPla.objects.filter(grade=grades)


class GunPlaDetailAPIView(RetrieveAPIView):
    """
        detail view of gundams
    """
    serializer_class = GunPlaDetailSerializer
    permission_class = [
        AllowAny
    ]
    #queryset = GunPla.objects.all()

    lookup_field = 'number'

    def get_queryset(self):
        grade = self.kwargs['grade']
        grades = Grade.objects.get(short=grade)
        return GunPla.objects.filter(grade=grades)


"""
    home detail
"""


class HomeDetailAPIView(RetrieveAPIView):
    """
        main home view
    """

    serializer_class = HomeDetailSerializer
    permission_class = [
        AllowAny
    ]
    queryset = GunPla

    def retrieve(self, request, pk=None):
        queryset = GunPla.objects.order_by('?').first()
        # queryset = queryset.get(pk=1)

        serializer = HomeDetailSerializer(queryset)
        return Response(serializer.data)