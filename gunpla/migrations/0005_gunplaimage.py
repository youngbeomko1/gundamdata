# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import gunpla.models


class Migration(migrations.Migration):

    dependencies = [
        ('gunpla', '0004_gunpla_gundam'),
    ]

    operations = [
        migrations.CreateModel(
            name='GunplaImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=gunpla.models.gunpla_image_model_upload_location)),
                ('image_thumb', models.ImageField(upload_to=gunpla.models.gunpla_image_model_upload_location)),
                ('gunpla', models.ForeignKey(to='gunpla.GunPla')),
            ],
        ),
    ]
