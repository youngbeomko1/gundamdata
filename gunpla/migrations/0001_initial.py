# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import gunpla.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='GundamModel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('image', models.ImageField(default=b'img/gundam/default_gundam.jpg', null=True, upload_to=gunpla.models.gundam_image_upload_location, blank=True)),
                ('image_thumbnail', models.ImageField(default=b'img/gundam/default_gundam.jpg', null=True, upload_to=gunpla.models.gundam_image_upload_location, blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='GundamSeries',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=150)),
                ('year_start', models.IntegerField(null=True, blank=True)),
                ('year_end', models.IntegerField(null=True, blank=True)),
                ('media', models.CharField(blank=True, max_length=30, null=True, choices=[(b'TV/OVA', b'TV/OVA'), (b'Movies', b'Movies'), (b'Manga', b'Manga'), (b'Novel', b'Novel'), (b'Game', b'Game'), (b'Other', b'Other'), (None, b'-')])),
            ],
        ),
        migrations.CreateModel(
            name='GundamUniverse',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('universe', models.CharField(max_length=100)),
            ],
        ),
        migrations.AddField(
            model_name='gundamseries',
            name='universe',
            field=models.ForeignKey(blank=True, to='gunpla.GundamUniverse', null=True),
        ),
        migrations.AddField(
            model_name='gundammodel',
            name='series',
            field=models.ForeignKey(blank=True, to='gunpla.GundamSeries', null=True),
        ),
        migrations.AddField(
            model_name='gundammodel',
            name='universe',
            field=models.ForeignKey(blank=True, to='gunpla.GundamUniverse', null=True),
        ),
    ]
