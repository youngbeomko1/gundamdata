# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gunpla', '0005_gunplaimage'),
    ]

    operations = [
        migrations.AddField(
            model_name='gunplaimage',
            name='order',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
