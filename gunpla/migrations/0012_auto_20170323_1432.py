# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gunpla', '0011_remove_grade_grade'),
    ]

    operations = [
        migrations.AddField(
            model_name='gunpla',
            name='accessories',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='gunpla',
            name='articulation',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='gunpla',
            name='building',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='gunpla',
            name='built',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='gunpla',
            name='custom',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='gunpla',
            name='design',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='gunpla',
            name='feature',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='gunpla',
            name='overall',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='gunpla',
            name='purchased',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='gunpla',
            name='quality',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='gunpla',
            name='skip',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='gunpla',
            name='wishlist',
            field=models.IntegerField(default=0),
        ),
    ]
