# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gunpla', '0002_auto_20170211_0610'),
    ]

    operations = [
        migrations.AlterField(
            model_name='grade',
            name='series',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
