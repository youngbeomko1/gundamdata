# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gunpla', '0012_auto_20170323_1432'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gunpla',
            name='overall',
            field=models.FloatField(default=0),
        ),
    ]
