# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gunpla', '0009_remove_gunpla_grades'),
    ]

    operations = [
        migrations.AddField(
            model_name='grade',
            name='grades',
            field=models.CharField(default='Master Grade', max_length=100),
            preserve_default=False,
        ),
    ]
