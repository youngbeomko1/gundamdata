# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gunpla', '0013_auto_20170324_1847'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gunpla',
            name='accessories',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='gunpla',
            name='articulation',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='gunpla',
            name='avg_rating',
            field=models.FloatField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='gunpla',
            name='design',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='gunpla',
            name='feature',
            field=models.FloatField(default=0),
        ),
        migrations.AlterField(
            model_name='gunpla',
            name='quality',
            field=models.FloatField(default=0),
        ),
    ]
