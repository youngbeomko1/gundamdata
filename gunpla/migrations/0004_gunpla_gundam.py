# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gunpla', '0003_auto_20170211_0612'),
    ]

    operations = [
        migrations.AddField(
            model_name='gunpla',
            name='gundam',
            field=models.ForeignKey(blank=True, to='gunpla.GundamModel', null=True),
        ),
    ]
