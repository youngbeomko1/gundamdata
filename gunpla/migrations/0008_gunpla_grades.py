# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gunpla', '0007_auto_20170211_1037'),
    ]

    operations = [
        migrations.AddField(
            model_name='gunpla',
            name='grades',
            field=models.ForeignKey(related_name='kit_grades', blank=True, to='gunpla.Grade', null=True),
        ),
    ]
