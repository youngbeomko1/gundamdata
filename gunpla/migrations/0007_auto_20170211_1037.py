# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gunpla', '0006_gunplaimage_order'),
    ]

    operations = [
        migrations.AlterField(
            model_name='gundammodel',
            name='series',
            field=models.ForeignKey(related_name='gundam_series', blank=True, to='gunpla.GundamSeries', null=True),
        ),
        migrations.AlterField(
            model_name='gundammodel',
            name='universe',
            field=models.ForeignKey(related_name='gundam_universe', blank=True, to='gunpla.GundamUniverse', null=True),
        ),
        migrations.AlterField(
            model_name='gundamseries',
            name='universe',
            field=models.ForeignKey(related_name='gundamseries_universe', blank=True, to='gunpla.GundamUniverse', null=True),
        ),
        migrations.AlterField(
            model_name='gunpla',
            name='grade',
            field=models.ForeignKey(related_name='kit_grade', blank=True, to='gunpla.Grade', null=True),
        ),
        migrations.AlterField(
            model_name='gunpla',
            name='gundam',
            field=models.ForeignKey(related_name='gundam_model', blank=True, to='gunpla.GundamModel', null=True),
        ),
        migrations.AlterField(
            model_name='gunplaimage',
            name='gunpla',
            field=models.ForeignKey(related_name='GunPla_kit', to='gunpla.GunPla'),
        ),
    ]
