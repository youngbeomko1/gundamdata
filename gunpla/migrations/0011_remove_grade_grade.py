# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gunpla', '0010_grade_grades'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='grade',
            name='grade',
        ),
    ]
