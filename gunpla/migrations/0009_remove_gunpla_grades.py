# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gunpla', '0008_gunpla_grades'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='gunpla',
            name='grades',
        ),
    ]
