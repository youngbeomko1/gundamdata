# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import gunpla.models


class Migration(migrations.Migration):

    dependencies = [
        ('gunpla', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Grade',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('grade', models.CharField(max_length=100)),
                ('short', models.CharField(max_length=20)),
                ('series', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='GunPla',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('number', models.IntegerField()),
                ('name', models.CharField(max_length=100)),
                ('short_name', models.CharField(max_length=100)),
                ('date', models.DateField()),
                ('price', models.IntegerField()),
                ('released', models.BooleanField(default=True)),
                ('image', models.ImageField(upload_to=gunpla.models.gunpla_image_upload_location)),
                ('image_thumb', models.ImageField(null=True, upload_to=gunpla.models.gunpla_image_upload_location, blank=True)),
                ('box_image', models.ImageField(null=True, upload_to=gunpla.models.gunpla_image_upload_location, blank=True)),
                ('box_thumb', models.ImageField(null=True, upload_to=gunpla.models.gunpla_image_upload_location, blank=True)),
                ('count_build', models.IntegerField(default=0)),
                ('avg_rating', models.IntegerField(null=True, blank=True)),
                ('grade', models.ForeignKey(blank=True, to='gunpla.Grade', null=True)),
            ],
        ),
        migrations.AlterField(
            model_name='gundammodel',
            name='image_thumbnail',
            field=models.ImageField(null=True, upload_to=gunpla.models.gundam_image_upload_location, blank=True),
        ),
        migrations.AlterUniqueTogether(
            name='gunpla',
            unique_together=set([('number', 'grade')]),
        ),
    ]
