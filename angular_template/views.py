from django.shortcuts import render

import os

from django.conf import settings
from django.http import Http404, HttpResponse

from django.views.generic import  View

# Create your views here.


class AngularTemplateView(View):
    def get(self, request, item=None, *args, **kwargs):
        template_path = settings.TEMPLATES[0]['DIRS'][0]
        item = os.path.join(template_path, "angular", 'gundamDB', item + '.html')

        try:
            html = open(item)
            return HttpResponse(html)
        except:
            raise Http404
